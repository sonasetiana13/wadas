import 'package:get/get.dart';
import 'package:take_home_test/repository/company_repository.dart';
import 'package:take_home_test/repository/product_repository.dart';
import 'package:take_home_test/repository/transaction_repository.dart';
import 'package:take_home_test/repository/user_repository.dart';

abstract class BaseController extends GetxController {
  late final userRepo = UserRepository();
  late final productRepo = ProductRepository();
  late final companyRepo = CompanyRepository();
  late final transactionRepo = TransactionRepository();
}
