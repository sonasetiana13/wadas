import 'package:flutter/material.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';

class TextArea extends StatelessWidget {
  const TextArea(
    this.controller, {
    Key? key,
    this.labelText,
    this.hintText,
    this.errorText,
    this.helperText,
    this.maxCharacters = 0,
  }) : super(key: key);

  final TextEditingController controller;
  final String? labelText;
  final String? hintText;
  final String? errorText;
  final String? helperText;
  final int maxCharacters;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      mainAxisSize: MainAxisSize.min,
      children: [
        Visibility(
          visible: labelText != null,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 8),
            child: Text(labelText ?? '', style: AppStyles.labelBlack2),
          ),
        ),
        SizedBox(
          height: 114,
          child: TextField(
            controller: controller,
            cursorColor: AppColors.yellow,
            maxLines: 100,
            style: AppStyles.subTextBlack,
            decoration: InputDecoration(
              hintText: hintText,
              contentPadding: const EdgeInsets.all(12),
              filled: true,
              fillColor: AppColors.white2,
              enabledBorder: _buildEnabledBorder(),
              focusedBorder: _buildFocusedBorder(),
            ),
          ),
        ),
        Visibility(
          visible: helperText != null || maxCharacters > 0,
          child: Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                _bottomInfoText,
                ValueListenableBuilder<TextEditingValue>(
                  valueListenable: controller,
                  builder: (context, value, child) => Text(
                    '${value.text.length}/$maxCharacters',
                    style: AppStyles.misc.update(color: AppColors.yellow),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  Text get _bottomInfoText {
    return errorText != null
        ? Text(errorText ?? '',
            style: AppStyles.misc.update(color: AppColors.red))
        : Text(helperText ?? '', style: AppStyles.miscBlack3);
  }

  OutlineInputBorder _buildEnabledBorder() {
    if (errorText != null) {
      return OutlineInputBorder(
        borderRadius: BorderRadius.circular(6),
        borderSide: const BorderSide(color: AppColors.red),
      );
    } else {
      return OutlineInputBorder(
        borderRadius: BorderRadius.circular(6),
        borderSide: const BorderSide(color: AppColors.black4),
      );
    }
  }

  OutlineInputBorder _buildFocusedBorder() {
    if (errorText != null) {
      return OutlineInputBorder(
        borderRadius: BorderRadius.circular(6),
        borderSide: const BorderSide(color: AppColors.red),
      );
    } else {
      return OutlineInputBorder(
        borderRadius: BorderRadius.circular(6),
        borderSide: const BorderSide(color: AppColors.black2),
      );
    }
  }
}
