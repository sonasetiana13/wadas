import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/themes/spacing.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/button.dart';

Future showConfirmDialog({
  String? title,
  String? message,
  String? labelButton,
  VoidCallback? callback,
}) =>
    Get.dialog(ComfirmDialog(
      message: message,
      title: title,
      labelButton: labelButton,
      callback: callback,
    ));

class ComfirmDialog extends StatelessWidget {
  final String? title, message, labelButton;
  final VoidCallback? callback;
  const ComfirmDialog({
    this.title,
    this.message,
    this.labelButton,
    this.callback,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
        alignment: Alignment.center,
        child: Container(
          height: 200,
          margin: const EdgeInsets.all(Spacing.medium),
          padding: const EdgeInsets.all(Spacing.medium),
          decoration: BoxDecoration(
              color: AppColors.white,
              borderRadius: BorderRadius.all(Radius.circular(Spacing.medium))),
          child: Material(
            color: Colors.transparent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  title ?? '',
                  style: AppStyles.title,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                Spacing.vSpace8,
                Text(
                  message ?? '',
                  style: AppStyles.subText,
                  textAlign: TextAlign.center,
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                ),
                Spacing.vSpace32,
                Row(
                  children: [
                    Expanded(
                      flex: 1,
                      child: Button.primary(
                        title: 'BATAL',
                        onPressed: () {
                          dissmis();
                        },
                      ),
                    ),
                    Spacing.hSpace16,
                    Expanded(
                      flex: 1,
                      child: Button.secondary(
                        title: labelButton ?? 'YA',
                        onPressed: callback ??
                            () {
                              dissmis();
                            },
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ));
  }
}
