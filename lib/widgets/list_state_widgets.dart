import 'package:flutter/material.dart';
import 'package:take_home_test/themes/app_colors.dart';

class ListEmptyWidget extends StatelessWidget {
  const ListEmptyWidget({Key? key, this.message, this.height = 0})
      : super(key: key);

  final double height;
  final String? message;

  @override
  Widget build(BuildContext context) {
    final widget = Center(child: Text(message ?? 'Tidak ada data'));
    if (height > 0) {
      return SizedBox(height: height, child: widget);
    } else {
      return widget;
    }
  }
}

class ListErrorWidget extends StatelessWidget {
  const ListErrorWidget(this.error, {Key? key, this.height = 0})
      : super(key: key);

  final String? error;
  final double height;

  @override
  Widget build(BuildContext context) {
    final widget = Center(child: Text(error ?? ''));
    if (height > 0) {
      return SizedBox(height: height, child: widget);
    } else {
      return widget;
    }
  }
}

class ListLoadingWidget extends StatelessWidget {
  const ListLoadingWidget({Key? key, this.height = 0}) : super(key: key);

  final double height;

  @override
  Widget build(BuildContext context) {
    const _indicator = Center(
        child: CircularProgressIndicator(
      color: AppColors.green,
      strokeWidth: 2,
    ));
    if (height > 0) {
      return SizedBox(height: height, child: _indicator);
    } else {
      return _indicator;
    }
  }
}
