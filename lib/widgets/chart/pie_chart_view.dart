import 'dart:math';

import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:take_home_test/db/data/transaction_model.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/spacing.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/widgets/chart/indicator.dart';

class PieChartView extends StatefulWidget {
  final List<TransactionSummary>? items;
  const PieChartView({Key? key, this.items}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _PieChartViewState();
}

class _PieChartViewState extends State<PieChartView> {
  int touchedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 1.3,
      child: Column(
        children: [
          Expanded(
            child: AspectRatio(
              aspectRatio: 1,
              child: PieChart(
                PieChartData(
                    pieTouchData: PieTouchData(
                        touchCallback: (FlTouchEvent event, pieTouchResponse) {
                      setState(() {
                        if (!event.isInterestedForInteractions ||
                            pieTouchResponse == null ||
                            pieTouchResponse.touchedSection == null) {
                          touchedIndex = -1;
                          return;
                        }
                        touchedIndex = pieTouchResponse
                            .touchedSection!.touchedSectionIndex;
                      });
                    }),
                    borderData: FlBorderData(
                      show: false,
                    ),
                    sectionsSpace: 0,
                    centerSpaceRadius: 30,
                    sections: showingSections()),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(Spacing.medium),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: List.generate(widget.items!.length, (index) {
                final item = widget.items![index];
                return Padding(
                  padding: const EdgeInsets.only(bottom: Spacing.small),
                  child: Indicator(
                    color: item.color ?? AppColors.green,
                    text: item.name ?? '',
                    isSquare: true,
                  ),
                );
              }),
            ),
          ),
          const SizedBox(
            width: 28,
          ),
        ],
      ),
    );
  }

  List<PieChartSectionData> showingSections() {
    return List.generate(widget.items!.length, (i) {
      final isTouched = i == touchedIndex;
      final fontSize = isTouched ? 20.0 : 12.0;
      final radius = isTouched ? 60.0 : 50.0;
      final item = widget.items![i];
      return PieChartSectionData(
        color: item.color ?? AppColors.green,
        value: (item.price ?? 0).toDouble(),
        title: priceFormat(item.price),
        radius: radius,
        titleStyle: TextStyle(
            fontSize: fontSize,
            fontWeight: FontWeight.bold,
            color: const Color(0xffffffff)),
      );
    });
  }
}
