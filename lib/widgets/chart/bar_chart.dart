import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:take_home_test/db/data/transaction_model.dart';
import 'package:take_home_test/themes/app_colors.dart';

class BarChartView extends StatelessWidget {
  final List<TransactionSummary>? items;
  const BarChartView({Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BarChart(
      BarChartData(
        barTouchData: barTouchData,
        titlesData: titlesData,
        borderData: borderData,
        barGroups: barGroups(),
        gridData: FlGridData(show: false),
        alignment: BarChartAlignment.spaceAround,
        maxY: 1000000,
      ),
    );
  }

  BarTouchData get barTouchData => BarTouchData(
        enabled: false,
        touchTooltipData: BarTouchTooltipData(
          tooltipBgColor: Colors.transparent,
          tooltipPadding: const EdgeInsets.all(0),
          tooltipMargin: 8,
          getTooltipItem: (
            BarChartGroupData group,
            int groupIndex,
            BarChartRodData rod,
            int rodIndex,
          ) {
            return BarTooltipItem(
              rod.y.round().toString(),
              const TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            );
          },
        ),
      );

  FlTitlesData get titlesData => FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          getTextStyles: (context, value) => const TextStyle(
            color: Color(0xff7589a2),
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
          margin: 0,
          getTitles: (double value) {
            return ''; //items![value.toInt()].price.toString();
            // switch (value.toInt()) {
            //   case 0:
            //     return 'Mn';
            //   case 1:
            //     return 'Te';
            //   case 2:
            //     return 'Wd';
            //   case 3:
            //     return 'Tu';
            //   case 4:
            //     return 'Fr';
            //   case 5:
            //     return 'St';
            //   case 6:
            //     return 'Sn';
            //   default:
            //     return '';
            // }
          },
        ),
        leftTitles: SideTitles(showTitles: false),
        topTitles: SideTitles(showTitles: false),
        rightTitles: SideTitles(showTitles: false),
      );

  FlBorderData get borderData => FlBorderData(
        show: false,
      );

  List<BarChartGroupData> barGroups() {
    List<BarChartGroupData> data = [];
    items!.forEach((e) {
      final bar = BarChartGroupData(
        x: e.id ?? 0,
        barRods: [
          BarChartRodData(
              y: (e.price ?? 0).toDouble(),
              colors: [AppColors.green, Colors.lightBlueAccent])
        ],
        showingTooltipIndicators: [0],
      );
      data.add(bar);
    });
    return data;
  }
}
