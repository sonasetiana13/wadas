import 'package:flutter/material.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';

class AppTextField extends StatelessWidget {
  final String? labelText;
  final String? errorText;
  final Widget? suffixIcon;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final bool obscureText;
  final EdgeInsets? margin;
  final Function()? onTap;
  final Function(String)? onChange;
  final bool enabled;

  const AppTextField({
    Key? key,
    this.labelText,
    this.errorText,
    this.suffixIcon,
    this.keyboardType,
    this.controller,
    this.obscureText = false,
    this.margin,
    this.onTap,
    this.enabled = true,
    this.onChange,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _addMarginIfNeeded(_addTapIfNeeded(_buildTextField()));
  }

  Widget _addMarginIfNeeded(Widget child) {
    if (margin != null) {
      return Padding(padding: margin!, child: child);
    }
    return child;
  }

  Widget _addTapIfNeeded(Widget child) {
    if (onTap != null) {
      return GestureDetector(
        onTap: onTap,
        child: child,
      );
    }
    return child;
  }

  TextField _buildTextField() {
    return TextField(
      keyboardType: keyboardType,
      enabled: enabled && onTap == null,
      controller: controller,
      obscureText: obscureText,
      style: enabled ? AppStyles.text : AppStyles.textBlack3,
      cursorColor: AppColors.green,
      onChanged: onChange,
      decoration: InputDecoration(
        focusedBorder: const UnderlineInputBorder(
          borderSide: BorderSide(width: 1, color: AppColors.green),
        ),
        contentPadding: EdgeInsets.zero,
        suffixIconConstraints: const BoxConstraints(
          maxHeight: 35,
          maxWidth: 70,
          minWidth: 35,
        ),
        suffixIcon: suffixIcon,
        prefixIconConstraints: const BoxConstraints(
          maxHeight: 35,
          maxWidth: 70,
          minWidth: 35,
        ),
        labelText: labelText,
        labelStyle: AppStyles.text.update(color: AppColors.black3),
        errorText: errorText,
        errorStyle: AppStyles.label.update(color: AppColors.red),
        floatingLabelStyle: AppStyles.label.update(color: AppColors.black3),
      ),
    );
  }
}
