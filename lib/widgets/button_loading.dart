import 'package:flutter/material.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/widgets/button.dart';

enum ButtonType { PRIMARY, SECONDARY, TERTIARY, TEXT }

class ButtonLoading extends StatelessWidget {
  final Color? bgColor, pbColor;
  final bool? loading;
  final String? title;
  final double? width, height, radius;
  final ButtonType? type;
  final Function()? onPressed;

  const ButtonLoading({
    Key? key,
    this.bgColor,
    this.pbColor,
    this.width,
    this.height,
    this.title,
    this.loading,
    this.radius,
    this.onPressed,
    this.type,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? double.infinity,
      height: height ?? 48,
      decoration: BoxDecoration(
          color: bgColor ?? AppColors.green,
          borderRadius: BorderRadius.circular(radius ?? 12)),
      child: loading == true ? progressBar() : button(),
    );
  }

  Widget progressBar() {
    return Center(
      child: SizedBox(
        width: 24,
        height: 24,
        child: CircularProgressIndicator(
          strokeWidth: 1,
          color: pbColor ?? AppColors.white,
        ),
      ),
    );
  }

  Widget button() {
    if (type == ButtonType.TERTIARY) {
      return Button.tertiary(
        title: title ?? '',
        onPressed: onPressed,
      );
    } else if (type == ButtonType.SECONDARY) {
      return Button.secondary(
        title: title ?? '',
        onPressed: onPressed,
      );
    } else if (type == ButtonType.TEXT) {
      return Button.text(
        title: title ?? '',
        onPressed: onPressed,
      );
    } else {
      return Button.primary(
        title: title ?? '',
        onPressed: onPressed,
      );
    }
  }
}
