import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/themes/spacing.dart';

SystemUiOverlayStyle getSystemUiOverlay(
        {Color? statusBarColor, Brightness? statusBarIconColor}) =>
    SystemUiOverlayStyle(
        statusBarColor: statusBarColor ?? AppColors.green,
        statusBarIconBrightness: statusBarIconColor ?? Brightness.light);

class BlankAppBar extends AppBar {
  final Color? statusBarColor;
  final Brightness? statusBarIconColor;

  BlankAppBar({Key? key, this.statusBarColor, this.statusBarIconColor})
      : super(
          key: key,
          toolbarHeight: 0,
          elevation: 0.0,
          foregroundColor: AppColors.black,
          backgroundColor: AppColors.white,
          titleTextStyle: AppStyles.body,
          systemOverlayStyle: getSystemUiOverlay(
              statusBarColor: statusBarColor,
              statusBarIconColor: statusBarIconColor),
        );
}

class WhiteAppBar extends AppBar {
  final String? titleText;
  final double? appBarHeight;
  final String? iconNav;
  final TabBar? tabs;

  WhiteAppBar(
      {Key? key, this.titleText, this.appBarHeight, this.iconNav, this.tabs})
      : super(
          key: key,
          elevation: 0.5,
          foregroundColor: AppColors.black,
          backgroundColor: AppColors.white,
          titleTextStyle: AppStyles.body,
          toolbarHeight: appBarHeight ?? kToolbarHeight,
          leading: navigator!.canPop()
              ? IconButton(
                  splashRadius: 24,
                  onPressed: () {
                    Get.back(result: 'OK');
                  },
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: AppColors.black,
                  ))
              : null,
          title: Text(titleText ?? ''),
          systemOverlayStyle: getSystemUiOverlay(
              statusBarColor: AppColors.white,
              statusBarIconColor: Brightness.dark),
          bottom: tabs,
        );
}
