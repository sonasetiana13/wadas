import 'package:flutter/material.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/themes/spacing.dart';

const _buttonHeight = 48.0;
const _smallButtonHeight = 42.0;
const _xsButtonHeight = 34.0;

class Button extends StatelessWidget {
  const Button({
    Key? key,
    this.title,
    this.icon,
    this.onPressed,
    this.backgroundColor,
    this.foregroundColor,
    this.borderColor,
    this.textStyle,
    this.height,
    this.loading = false,
    this.interactionEnabled = true,
  }) : super(key: key);

  final String? title;
  final Widget? icon;
  final Function()? onPressed;
  final Color? backgroundColor;
  final Color? foregroundColor;
  final Color? borderColor;
  final TextStyle? textStyle;
  final double? height;
  final bool loading;
  final bool interactionEnabled;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? 44,
      child: TextButton(
        onPressed: onPressed,
        child: IgnorePointer(
          ignoring: !interactionEnabled,
          child: loading
              ? _loadingIndicator
              : _ButtonChildWidget(title: title, icon: icon),
        ),
        style: ButtonStyle(
          textStyle: MaterialStateProperty.all(textStyle),
          foregroundColor: MaterialStateProperty.resolveWith(
            (states) => _buildFgColor(states),
          ),
          backgroundColor: MaterialStateProperty.resolveWith(
            (states) => _buildBgColor(states),
          ),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8),
              side: _getBorder(),
            ),
          ),
        ),
      ),
    );
  }

  get _loadingIndicator => SizedBox(
        width: 24,
        height: 24,
        child: CircularProgressIndicator(
          color: foregroundColor,
          strokeWidth: 3,
        ),
      );

  BorderSide _getBorder() {
    return borderColor == null
        ? BorderSide.none
        : BorderSide(color: borderColor!, width: 1);
  }

  Color? _buildBgColor(Set<MaterialState> states) {
    if (states.contains(MaterialState.disabled)) {
      return AppColors.border;
    } else {
      return backgroundColor;
    }
  }

  Color? _buildFgColor(Set<MaterialState> states) {
    if (states.contains(MaterialState.disabled)) {
      return AppColors.black3;
    } else {
      return foregroundColor;
    }
  }

  const factory Button.primary({
    Key? key,
    String? title,
    Color? backgroundColor,
    Function()? onPressed,
    bool loading,
  }) = _PrimaryButton;

  const factory Button.secondary({
    Key? key,
    String? title,
    Widget? icon,
    Function()? onPressed,
    bool loading,
  }) = _SecondaryButton;

  const factory Button.tertiary({
    Key? key,
    String? title,
    Function()? onPressed,
  }) = _TertiaryButton;

  const factory Button.text({
    Key? key,
    String? title,
    Color? textColor,
    Function()? onPressed,
  }) = _TextButton;

  //small
  const factory Button.primarySmall({
    Key? key,
    String? title,
    Function()? onPressed,
    bool? loading,
  }) = _PrimaryButtonSmall;

  const factory Button.secondarySmall({
    Key? key,
    String? title,
    Widget? icon,
    Function()? onPressed,
  }) = _SecondaryButtonSmall;

  const factory Button.tertiarySmall({
    Key? key,
    String? title,
    Function()? onPressed,
  }) = _TertiaryButtonSmall;

  const factory Button.textSmall({
    Key? key,
    String? title,
    Color? textColor,
    Function()? onPressed,
  }) = _TextButtonSmall;

  //extra small
  const factory Button.primaryXS({
    Key? key,
    String? title,
    Function()? onPressed,
  }) = _PrimaryButtonXS;

  const factory Button.secondaryXS({
    Key? key,
    String? title,
    Widget? icon,
    Function()? onPressed,
  }) = _SecondaryButtonXS;

  const factory Button.tertiaryXS({
    Key? key,
    String? title,
    Function()? onPressed,
  }) = _TertiaryButtonXS;

  const factory Button.textXS({
    Key? key,
    String? title,
    Color? textColor,
    Function()? onPressed,
  }) = _TextButtonXS;
}

class _ButtonChildWidget extends StatelessWidget {
  final String? title;
  final Widget? icon;

  const _ButtonChildWidget({Key? key, this.title, this.icon}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (title == null) {
      return Spacing.zero;
    } else if (icon == null) {
      return Text(title!);
    } else {
      return Stack(
        alignment: Alignment.centerLeft,
        children: [
          Positioned.fill(
            child: Center(
              child: Text(title!, textAlign: TextAlign.center),
            ),
          ),
          Positioned(
            child: icon!,
            left: 0,
          ),
        ],
      );
    }
  }
}

//types
class _PrimaryButton extends Button {
  const _PrimaryButton({
    Key? key,
    String? title,
    Color? backgroundColor,
    Function()? onPressed,
    bool loading = false,
  }) : super(
          key: key,
          title: title,
          backgroundColor: backgroundColor ?? AppColors.green,
          foregroundColor: AppColors.white,
          textStyle: AppStyles.bodyBoldBlack,
          height: _buttonHeight,
          onPressed: onPressed,
          loading: loading,
          interactionEnabled: !loading,
        );
}

class _SecondaryButton extends Button {
  const _SecondaryButton({
    Key? key,
    String? title,
    Widget? icon,
    Function()? onPressed,
    bool loading = false,
  }) : super(
          key: key,
          title: title,
          icon: icon,
          backgroundColor: AppColors.white,
          foregroundColor: AppColors.black2,
          borderColor: AppColors.black4,
          textStyle: AppStyles.bodyBoldBlack,
          height: _buttonHeight,
          onPressed: onPressed,
          loading: loading,
          interactionEnabled: !loading,
        );
}

class _TertiaryButton extends Button {
  const _TertiaryButton({
    Key? key,
    String? title,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          backgroundColor: AppColors.yellow4,
          foregroundColor: AppColors.green,
          textStyle: AppStyles.bodyBoldBlack,
          height: _buttonHeight,
          onPressed: onPressed,
        );
}

class _TextButton extends Button {
  const _TextButton({
    Key? key,
    String? title,
    Color? textColor,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          foregroundColor: textColor ?? AppColors.brown,
          textStyle: AppStyles.bodyBoldBlack,
          height: _buttonHeight,
          onPressed: onPressed,
        );
}

//small buttons
class _PrimaryButtonSmall extends Button {
  const _PrimaryButtonSmall({
    Key? key,
    String? title,
    Function()? onPressed,
    bool? loading,
  }) : super(
          key: key,
          title: title,
          backgroundColor: AppColors.green,
          foregroundColor: AppColors.white,
          textStyle: AppStyles.textBoldBlack,
          height: _smallButtonHeight,
          onPressed: onPressed,
          loading: loading ?? false,
        );
}

class _SecondaryButtonSmall extends Button {
  const _SecondaryButtonSmall({
    Key? key,
    String? title,
    Widget? icon,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          icon: icon,
          backgroundColor: AppColors.white,
          foregroundColor: AppColors.black2,
          borderColor: AppColors.black4,
          textStyle: AppStyles.textBoldBlack,
          height: _smallButtonHeight,
          onPressed: onPressed,
        );
}

class _TertiaryButtonSmall extends Button {
  const _TertiaryButtonSmall({
    Key? key,
    String? title,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          backgroundColor: AppColors.yellow4,
          foregroundColor: AppColors.green,
          textStyle: AppStyles.textBoldBlack,
          height: _smallButtonHeight,
          onPressed: onPressed,
        );
}

class _TextButtonSmall extends Button {
  const _TextButtonSmall({
    Key? key,
    String? title,
    Color? textColor,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          foregroundColor: textColor ?? AppColors.brown,
          textStyle: AppStyles.textBoldBlack,
          height: _smallButtonHeight,
          onPressed: onPressed,
        );
}

//extra small buttons
class _PrimaryButtonXS extends Button {
  const _PrimaryButtonXS({
    Key? key,
    String? title,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          backgroundColor: AppColors.green,
          foregroundColor: AppColors.white,
          textStyle: AppStyles.textBoldBlack,
          height: _xsButtonHeight,
          onPressed: onPressed,
        );
}

class _SecondaryButtonXS extends Button {
  const _SecondaryButtonXS({
    Key? key,
    String? title,
    Widget? icon,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          icon: icon,
          backgroundColor: AppColors.white,
          foregroundColor: AppColors.black2,
          borderColor: AppColors.black4,
          textStyle: AppStyles.textBoldBlack,
          height: _xsButtonHeight,
          onPressed: onPressed,
        );
}

class _TertiaryButtonXS extends Button {
  const _TertiaryButtonXS({
    Key? key,
    String? title,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          backgroundColor: AppColors.yellow4,
          foregroundColor: AppColors.green,
          textStyle: AppStyles.textBoldBlack,
          height: _xsButtonHeight,
          onPressed: onPressed,
        );
}

class _TextButtonXS extends Button {
  const _TextButtonXS({
    Key? key,
    String? title,
    Color? textColor,
    Function()? onPressed,
  }) : super(
          key: key,
          title: title,
          foregroundColor: textColor ?? AppColors.brown,
          textStyle: AppStyles.textBoldBlack,
          height: _xsButtonHeight,
          onPressed: onPressed,
        );
}
