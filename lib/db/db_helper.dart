import 'package:sqflite/sqflite.dart' as sql;

class DBHelper {
  static Future<sql.Database> db() async {
    return sql.openDatabase(
      'takehometest.db',
      version: 1,
      onCreate: (sql.Database db, int version) async {
        await _createTableUsers(db);
        await _createTableBarang(db);
        await _createTablePerusahaan(db);
        await _createTableTransaction(db);
      },
    );
  }

  static Future<void> _createTableUsers(sql.Database db) async {
    await db.execute("""CREATE TABLE users(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        name TEXT,
        email TEXT,
        phone TEXT,
        password TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<void> _createTableBarang(sql.Database db) async {
    await db.execute("""CREATE TABLE barang(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        name TEXT,
        price INTEGER,
        stock INTEGER,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<void> _createTablePerusahaan(sql.Database db) async {
    await db.execute("""CREATE TABLE perusahaan(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        name TEXT,
        email TEXT,
        address TEXT,
        phone TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }

  static Future<void> _createTableTransaction(sql.Database db) async {
    await db.execute("""CREATE TABLE transaksi(
        id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        id_barang INTEGER,
        id_perusahaan INTEGER,
        trx_code TEXT,
        total_barang INTEGER,
        total_harga INTEGER,
        keterangan TEXT,
        createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
      )
      """);
  }
}
