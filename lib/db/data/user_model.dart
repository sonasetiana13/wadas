class UserModel {
  int? id;
  String? name, email, phone, password, createAt;

  UserModel({this.id, this.name, this.email, this.phone, this.createAt});

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'email': email,
        'phone': phone,
      };
}
