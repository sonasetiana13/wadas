import 'package:flutter/material.dart';

class TransactionModel {
  int? id;
  int? productId;
  int? companyId;
  String? trxCode;
  String? note;
  String? productName;
  String? companyName;
  int? totalProduct;
  int? totalPrice;
  String? createdAt;

  TransactionModel(
      {this.id,
      this.productId,
      this.companyId,
      this.trxCode,
      this.note,
      this.totalPrice,
      this.totalProduct,
      this.createdAt,
      this.productName,
      this.companyName});
}

class TransactionSummary {
  int? id;
  int? price;
  int? stock;
  String? name;
  String? createdAt;
  Color? color;

  TransactionSummary(
      {this.id, this.price, this.stock, this.name, this.createdAt, this.color});
}
