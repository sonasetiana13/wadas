class CompanyModel {
  int? id;
  String? name;
  String? email;
  String? address;
  String? phone;
  String? createAt;

  CompanyModel({
    this.id,
    this.name,
    this.email,
    this.phone,
    this.address,
    this.createAt,
  });
}
