class ProductModel {
  int? id;
  String? name;
  int? price;
  int? stock;
  String? createAt;

  ProductModel({
    this.id,
    this.name,
    this.price,
    this.stock,
    this.createAt,
  });
}
