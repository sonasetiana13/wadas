import 'package:intl/intl.dart';

String parseDate(String? value) {
  if (value == null) return '';
  final date = DateFormat('yyyy-MM-dd hh:mm:ss').parse(value);
  return DateFormat('yyyy-MM-dd').format(date);
}

String priceFormat(int? value) {
  if (value == null) return '0';
  final nf =
      NumberFormat.currency(locale: 'en_US', symbol: '', decimalDigits: 0);
  return nf.format(value);
}
