import 'dart:developer';
import 'dart:io';

//import 'package:fluttertoast/fluttertoast.dart';
import 'package:csv/csv.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

void dissmis() => navigator!.pop();

void back() => Get.back(result: 'OK');

String? TextFieldError(String error) {
  return error.isNotEmpty ? error : null;
}

void toast(String message) {
  // Fluttertoast.showToast(
  //     msg: message,
  //     gravity: ToastGravity.BOTTOM,
  //     toastLength: Toast.LENGTH_LONG);
}

void logI(String? message) {
  log('LogInfo: ${message ?? ''}');
}

void logE(String? message) {
  log('LogError: ${message ?? ''}');
}

void showErrorDatabase() {
  showMessageDialog(
    'Terjadi kesalhan pada database.',
    title: 'Error!',
  );
}

String generateTrxCode() {
  final dt = DateTime.now();
  return 'TRX${dt.year}${dt.month}${dt.day}${dt.hour}${dt.minute}${dt.second}';
}

Future<bool> _getStoragePermission() async {
  bool permissionGranted = false;
  if (await Permission.storage.request().isGranted) {
    permissionGranted = true;
  } else if (await Permission.storage.request().isPermanentlyDenied) {
    await openAppSettings();
  } else if (await Permission.storage.request().isDenied) {
    permissionGranted = false;
  }
  return permissionGranted;
}

Future<String> getFilePath(String fileName) async {
  final dir = await getExternalStorageDocDirectory();
  String filePath = '$dir/$fileName.csv';
  return filePath;
}

Future<String> readFile(String filePath) async {
  File file = File(filePath); // 1
  String content = await file.readAsString(); // 2

  return content;
}

Future<String> getExternalStorageDocDirectory() async {
  final Directory? extDir = await getExternalStorageDirectory();
  String dirPath = '${extDir?.path}/Documents';
  logI(dirPath);
  dirPath =
      dirPath.replaceAll("Android/data/com.example.take_home_test/files/", "");
  await Directory(dirPath).create(recursive: true);
  return dirPath;
}

Future<void> exportDataToCsv(List<List<dynamic>> data, String fileName) async {
  final requestPermission = await _getStoragePermission();

  if (requestPermission) {
    try {
      String csv = const ListToCsvConverter().convert(data);
      final path = await getFilePath(
          fileName + DateTime.now().microsecondsSinceEpoch.toString());
      File file = File(path);
      file.writeAsString(csv);
      final content = await readFile(path);

      if (content.isNotEmpty) {
        showMessageDialog('File tersimpan di ' + path,
            title: 'Data Berhasil Dicetak');
      } else {
        showMessageDialog('Terjadi kesalahan system',
            title: 'Data Gagal Dicetak');
      }
    } catch (e) {
      showMessageDialog('Tidak dapat mencetak file csv', title: 'Error!');
    }
  } else {
    showConfirmDialog(
        title: 'Ijin Ditolak!',
        message: 'Aplikasi membutuhkan ijin untuk akses pemyimpanan',
        labelButton: 'Beri Ijin',
        callback: () async {
          await openAppSettings();
        });
  }
}
