import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/modules/product/product_control.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/button.dart';

class ProductPage extends GetWidget<ProductController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        titleText: 'Data Barang',
      ),
      body: controller.obx((items) => _DataTable(controller, items: items)),
    );
  }
}

class _DataTable extends StatelessWidget {
  final ProductController c;
  final List<ProductModel>? items;
  const _DataTable(this.c, {Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaginatedDataTable(
      header: const Text(''),
      columnSpacing: 50,
      horizontalMargin: 10,
      rowsPerPage: 8,
      showCheckboxColumn: false,
      actions: [
        Button.primarySmall(
          title: 'Tambah Barang',
          onPressed: () async {
            await Get.toNamed(
              Routes.productForm,
              arguments: {
                'title': 'Tambah Barang',
                'button': 'Tambah',
                'type': 'create'
              },
            );
            c.getProducts();
          },
        )
      ],
      columns: const [
        DataColumn(label: Text('Tanggal')),
        DataColumn(label: Text('Nama Barang')),
        DataColumn(label: Text('Stok')),
        DataColumn(label: Text('Harga')),
        DataColumn(label: Text('Hapus')),
        DataColumn(label: Text('Edit'))
      ],
      source: DataSource(c, items: items),
    );
  }
}

class DataSource extends DataTableSource {
  final ProductController c;
  final List<ProductModel>? items;
  DataSource(this.c, {this.items});

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => (items ?? []).length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    final item = items![index];
    return DataRow(cells: [
      DataCell(Text(parseDate(item.createAt))),
      DataCell(Text(item.name ?? '')),
      DataCell(Text('${item.stock ?? 0}')),
      DataCell(Text(priceFormat(item.price))),
      DataCell(IconButton(
        onPressed: () {
          c.deleteProduct(item.id ?? 0);
        },
        icon: const Icon(
          Icons.delete,
          size: 16,
        ),
        splashRadius: 16,
      )),
      DataCell(IconButton(
        onPressed: () async {
          await Get.toNamed(
            Routes.productForm,
            arguments: {
              'title': 'Edit Barang',
              'button': 'Edit',
              'type': 'edit',
              'id': item.id,
              'name': item.name,
              'stock': item.stock,
              'price': item.price,
            },
          );
          c.getProducts();
        },
        icon: Icon(
          Icons.edit,
          size: 16,
        ),
        splashRadius: 16,
      ))
    ]);
  }
}
