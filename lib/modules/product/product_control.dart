import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class ProductController extends BaseController
    with StateMixin<List<ProductModel>> {
  @override
  void onReady() {
    super.onReady();
    getProducts();
  }

  Future<void> getProducts() async {
    logI('getProducts');
    try {
      final results = await productRepo.read();
      change(results, status: RxStatus.success());
    } catch (e) {
      showErrorDatabase();
    }
  }

  Future<void> deleteProduct(int id) async {
    showConfirmDialog(
      title: 'Hapus!',
      message: 'Apakah Anda yakin ingin menghapus?',
      callback: () async {
        dissmis();
        try {
          await productRepo.delete(id);
          showMessageDialog("Barang berhasil dihapus.", title: 'Hapus!');
          getProducts();
        } catch (e) {
          showErrorDatabase();
        }
      },
    );
  }
}
