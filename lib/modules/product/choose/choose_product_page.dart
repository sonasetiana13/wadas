import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/modules/product/choose/choose_product_control.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/list_state_widgets.dart';

class ChooseProductPage extends GetWidget<ChooseProductController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: WhiteAppBar(
        titleText: 'Pilih Barang',
      ),
      body: controller.obx(
          (items) => ListView.builder(
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (ctx, index) {
                  final item = items![index];
                  return _ItemProduct(item: item);
                },
                itemCount: items!.length,
              ),
          onEmpty: const ListEmptyWidget()),
    );
  }
}

class _ItemProduct extends StatelessWidget {
  final ProductModel? item;
  const _ItemProduct({Key? key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text(
            item?.name ?? '',
            style: AppStyles.body,
          ),
          subtitle: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                'Rp ${priceFormat(item?.price)}',
                style: AppStyles.text.update(color: AppColors.green),
              ),
              Text(
                'Stok ${item?.stock ?? 0}',
                style: AppStyles.subText,
              )
            ],
          ),
          trailing: Icon(
            Icons.chevron_right_outlined,
            color: AppColors.green,
          ),
          onTap: () {
            if ((item?.stock ?? 0) > 0) {
              Get.back(result: item);
            } else {
              Get.snackbar('Barang', 'Stok barang kosong');
            }
          },
        ),
        Divider(
          thickness: 1,
        ),
      ],
    );
  }
}
