import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/utilitys/view_utils.dart';

class ChooseProductController extends BaseController
    with StateMixin<List<ProductModel>> {
  @override
  void onReady() {
    super.onReady();
    getProducts();
  }

  Future<void> getProducts() async {
    try {
      final results = await productRepo.read();
      change(results,
          status: results.isEmpty ? RxStatus.empty() : RxStatus.success());
    } catch (e) {
      showErrorDatabase();
    }
  }
}
