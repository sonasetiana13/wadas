import 'package:get/get.dart';
import 'package:take_home_test/modules/product/choose/choose_product_control.dart';

class ChooseProductBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => ChooseProductController());
  }
}
