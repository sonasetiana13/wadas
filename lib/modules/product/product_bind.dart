import 'package:get/get.dart';
import 'package:take_home_test/modules/product/product_control.dart';

class ProductBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => ProductController());
  }
}
