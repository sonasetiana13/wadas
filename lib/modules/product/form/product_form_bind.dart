import 'package:get/get.dart';
import 'package:take_home_test/modules/product/form/product_form_control.dart';

class FormProductBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => FormProductController());
  }
}
