import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class FormProductController extends BaseController {
  final nameControl = TextEditingController();
  final priceControl = TextEditingController();
  final stockControl = TextEditingController();

  bool _isTypeEdit = false;

  final errorName = ''.obs;
  final errorPrice = ''.obs;
  final errorStock = ''.obs;

  @override
  void onInit() {
    _isTypeEdit = Get.arguments['type'] == 'edit';
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (_isTypeEdit) {
      nameControl.text = Get.arguments['name'];
      priceControl.text = Get.arguments['price'].toString();
      stockControl.text = Get.arguments['stock'].toString();
    }
  }

  @override
  void dispose() {
    super.dispose();
    nameControl.dispose();
    priceControl.dispose();
    stockControl.dispose();
  }

  Future<void> save() async {
    final name = nameControl.text;
    final stock = stockControl.text;
    final price = priceControl.text;

    errorName.value = '';
    errorPrice.value = '';
    errorStock.value = '';

    if (name.isEmpty) {
      errorName.value = 'Nama barang tidak boleh kosong';
      return;
    }

    if (stock.isEmpty) {
      errorStock.value = 'Stok tidak boleh kosong';
      return;
    }

    if (price.isEmpty) {
      errorPrice.value = 'Harga tidak boleh kosong';
      return;
    }

    showConfirmDialog(
        title: 'Konfirmasi!',
        message: 'Pastikan data yang Anda masukkan sudah benar.',
        labelButton: 'Ya',
        callback: () async {
          dissmis();
          try {
            bool result = false;
            if (_isTypeEdit) {
              result = await productRepo.update(Get.arguments['id'], name,
                  int.parse(price), int.parse(stock));
            } else {
              result = await productRepo.create(
                  name, int.parse(price), int.parse(stock));
            }

            if (result) {
              if (!_isTypeEdit) {
                nameControl.text = '';
                priceControl.text = '';
                stockControl.text = '';
              }
              showMessageDialog(
                  'Barang berhasil ${_isTypeEdit ? 'diedit' : 'disimpan'}.',
                  title: _isTypeEdit ? 'Edit!' : 'Simpan!');
            } else {
              showErrorDatabase();
            }
          } catch (e) {
            logE(e.toString());
            showErrorDatabase();
          }
        });
  }
}
