import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/product/form/product_form_control.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/spacing.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/button.dart';
import 'package:take_home_test/widgets/textfield.dart';

class FormProductPage extends GetWidget<FormProductController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.white,
        appBar: WhiteAppBar(
          titleText: Get.arguments['title'],
        ),
        body: Padding(
          padding: const EdgeInsets.all(Spacing.medium),
          child: Column(
            children: [
              Obx(() => AppTextField(
                    labelText: 'Nama Barang',
                    keyboardType: TextInputType.text,
                    controller: controller.nameControl,
                    errorText: TextFieldError(controller.errorName.value),
                  )),
              Spacing.vSpace24,
              Obx(() => AppTextField(
                    labelText: 'Stok',
                    keyboardType: TextInputType.number,
                    controller: controller.stockControl,
                    errorText: TextFieldError(controller.errorStock.value),
                  )),
              Spacing.vSpace24,
              Obx(() => AppTextField(
                    labelText: 'Harga',
                    controller: controller.priceControl,
                    errorText: TextFieldError(controller.errorPrice.value),
                    keyboardType: TextInputType.number,
                  )),
              Spacer(),
              SizedBox(
                  width: double.infinity,
                  child: Button.primary(
                    title: Get.arguments['button'],
                    onPressed: () {
                      controller.save();
                    },
                  )),
            ],
          ),
        ));
  }
}
