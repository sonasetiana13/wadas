import 'package:get/get.dart';
import 'package:take_home_test/modules/company/company_control.dart';

class CompanyBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => CompanyController());
  }
}
