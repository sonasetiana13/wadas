import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/company_model.dart';
import 'package:take_home_test/modules/company/company_control.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/button.dart';

class CompanyPage extends GetWidget<CompanyController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        titleText: 'Data Perusahaan',
      ),
      body: controller
          .obx((items) => _DataTableCompany(controller, items: items)),
    );
  }
}

class _DataTableCompany extends StatelessWidget {
  final CompanyController c;
  final List<CompanyModel>? items;
  const _DataTableCompany(this.c, {Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaginatedDataTable(
      header: const Text(''),
      columnSpacing: 50,
      horizontalMargin: 10,
      rowsPerPage: 8,
      showCheckboxColumn: false,
      actions: [
        Button.primarySmall(
          title: 'Tambah Perusahaan',
          onPressed: () async {
            await Get.toNamed(
              Routes.companyForm,
              arguments: {
                'title': 'Tambah Perusahaan',
                'button': 'Tambah',
                'type': 'create'
              },
            );
            c.getCompanys();
          },
        )
      ],
      columns: const [
        DataColumn(label: Text('Tanggal')),
        DataColumn(label: Text('Nama Perusahaan')),
        DataColumn(label: Text('Email')),
        DataColumn(label: Text('No Telepon')),
        DataColumn(label: Text('Alamat')),
        DataColumn(label: Text('Hapus')),
        DataColumn(label: Text('Edit'))
      ],
      source: _DataSourceCompany(c, items: items),
    );
  }
}

class _DataSourceCompany extends DataTableSource {
  final CompanyController c;
  final List<CompanyModel>? items;
  _DataSourceCompany(this.c, {this.items});

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => (items ?? []).length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    final item = items![index];
    return DataRow(cells: [
      DataCell(Text(parseDate(item.createAt))),
      DataCell(Text(item.name ?? '')),
      DataCell(Text(item.email ?? '')),
      DataCell(Text(item.phone ?? '')),
      DataCell(Text(item.address ?? '')),
      DataCell(IconButton(
        onPressed: () {
          c.deleteCompany(item.id ?? 0);
        },
        icon: const Icon(
          Icons.delete,
          size: 16,
        ),
        splashRadius: 16,
      )),
      DataCell(IconButton(
        onPressed: () async {
          await Get.toNamed(
            Routes.companyForm,
            arguments: {
              'title': 'Edit Barang',
              'button': 'Edit',
              'type': 'edit',
              'id': item.id,
              'name': item.name,
              'email': item.email,
              'phone': item.phone,
              'address': item.address,
            },
          );
          c.getCompanys();
        },
        icon: const Icon(
          Icons.edit,
          size: 16,
        ),
        splashRadius: 16,
      ))
    ]);
  }
}
