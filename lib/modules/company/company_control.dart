import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/db/data/company_model.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class CompanyController extends BaseController
    with StateMixin<List<CompanyModel>> {
  @override
  void onReady() {
    super.onReady();
    getCompanys();
  }

  Future<void> getCompanys() async {
    try {
      final results = await companyRepo.read();
      change(results, status: RxStatus.success());
    } catch (e) {
      showErrorDatabase();
    }
  }

  Future<void> deleteCompany(int id) async {
    showConfirmDialog(
      title: 'Hapus!',
      message: 'Apakah Anda yakin ingin menghapus?',
      callback: () async {
        dissmis();
        try {
          await companyRepo.delete(id);
          showMessageDialog("Perusahaan berhasil dihapus.", title: 'Hapus!');
          getCompanys();
        } catch (e) {
          showErrorDatabase();
        }
      },
    );
  }
}
