import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/company/form/company_form_control.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/spacing.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/button.dart';
import 'package:take_home_test/widgets/textarea.dart';
import 'package:take_home_test/widgets/textfield.dart';

class FormCompanyPage extends GetWidget<FormCompanyController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.white,
        appBar: WhiteAppBar(
          titleText: Get.arguments['title'],
        ),
        body: Padding(
          padding: const EdgeInsets.all(Spacing.medium),
          child: Column(
            children: [
              Obx(() => AppTextField(
                    labelText: 'Nama Perusahaan',
                    keyboardType: TextInputType.text,
                    controller: controller.nameControl,
                    errorText: TextFieldError(controller.errorName.value),
                  )),
              Spacing.vSpace24,
              Obx(() => AppTextField(
                    labelText: 'Email',
                    keyboardType: TextInputType.emailAddress,
                    controller: controller.emailControl,
                    errorText: TextFieldError(controller.errorEmail.value),
                  )),
              Spacing.vSpace24,
              Obx(() => AppTextField(
                    labelText: 'No Telepon',
                    controller: controller.phoneControl,
                    errorText: TextFieldError(controller.errorPhone.value),
                    keyboardType: TextInputType.phone,
                  )),
              Spacing.vSpace24,
              Obx(() => AppTextField(
                    controller: controller.addressControl,
                    labelText: 'Alamat',
                    errorText: TextFieldError(controller.errorAddress.value),
                    keyboardType: TextInputType.streetAddress,
                  )),
              Spacer(),
              SizedBox(
                  width: double.infinity,
                  child: Button.primary(
                    title: Get.arguments['button'],
                    onPressed: () {
                      controller.save();
                    },
                  )),
            ],
          ),
        ));
  }
}
