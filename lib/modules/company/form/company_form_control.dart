import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class FormCompanyController extends BaseController {
  final nameControl = TextEditingController();
  final emailControl = TextEditingController();
  final phoneControl = TextEditingController();
  final addressControl = TextEditingController();

  bool _isTypeEdit = false;

  final errorName = ''.obs;
  final errorEmail = ''.obs;
  final errorPhone = ''.obs;
  final errorAddress = ''.obs;

  @override
  void onInit() {
    _isTypeEdit = Get.arguments['type'] == 'edit';
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (_isTypeEdit) {
      nameControl.text = Get.arguments['name'];
      emailControl.text = Get.arguments['email'];
      phoneControl.text = Get.arguments['phone'];
      addressControl.text = Get.arguments['address'];
    }
  }

  @override
  void dispose() {
    super.dispose();
    nameControl.dispose();
    emailControl.dispose();
    phoneControl.dispose();
    addressControl.dispose();
  }

  Future<void> save() async {
    final name = nameControl.text;
    final email = emailControl.text;
    final phone = phoneControl.text;
    final address = addressControl.text;

    errorName.value = '';
    errorEmail.value = '';
    errorPhone.value = '';
    errorAddress.value = '';

    if (name.isEmpty) {
      errorName.value = 'Nama perusahaan tidak boleh kosong';
      return;
    }

    if (email.isEmpty) {
      errorEmail.value = 'Email tidak boleh kosong';
      return;
    }

    if (phone.isEmpty) {
      errorPhone.value = 'No telepon tidak boleh kosong';
      return;
    }

    if (address.isEmpty) {
      errorAddress.value = 'Alamat tidak boleh kosong';
      return;
    }

    showConfirmDialog(
        title: 'Konfirmasi!',
        message: 'Pastikan data yang Anda masukkan sudah benar.',
        labelButton: 'Ya',
        callback: () async {
          dissmis();
          try {
            bool result = false;
            if (_isTypeEdit) {
              result = await companyRepo.update(
                  Get.arguments['id'], name, email, phone, address);
            } else {
              result = await companyRepo.create(name, email, phone, address);
            }

            if (result) {
              if (!_isTypeEdit) {
                nameControl.text = '';
                emailControl.text = '';
                phoneControl.text = '';
                addressControl.text = '';
              }
              showMessageDialog(
                  'Perusahaan berhasil ${_isTypeEdit ? 'diedit' : 'disimpan'}.',
                  title: _isTypeEdit ? 'Edit!' : 'Simpan!');
            } else {
              showErrorDatabase();
            }
          } catch (e) {
            logE(e.toString());
            showErrorDatabase();
          }
        });
  }
}
