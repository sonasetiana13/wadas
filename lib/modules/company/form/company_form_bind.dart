import 'package:get/get.dart';
import 'package:take_home_test/modules/company/form/company_form_control.dart';

class FormCompanyBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => FormCompanyController());
  }
}
