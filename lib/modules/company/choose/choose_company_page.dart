import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/company_model.dart';
import 'package:take_home_test/modules/company/choose/choose_company_control.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/list_state_widgets.dart';

class ChooseCompanyPage extends GetWidget<ChooseCompanyController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: WhiteAppBar(
        titleText: 'Pilih Perusahaan',
      ),
      body: controller.obx(
          (items) => ListView.builder(
                physics: const AlwaysScrollableScrollPhysics(),
                itemBuilder: (ctx, index) {
                  final item = items![index];
                  return _ItemCompany(item: item);
                },
                itemCount: items!.length,
              ),
          onEmpty: const ListEmptyWidget()),
    );
  }
}

class _ItemCompany extends StatelessWidget {
  final CompanyModel? item;
  const _ItemCompany({Key? key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        ListTile(
          title: Text(
            item?.name ?? '',
            style: AppStyles.body,
          ),
          subtitle: Text(
            item?.address ?? '',
            style: AppStyles.text,
          ),
          trailing: const Icon(
            Icons.chevron_right_outlined,
            color: AppColors.green,
          ),
          onTap: () {
            Get.back(result: item);
          },
        ),
        Divider(
          thickness: 1,
        ),
      ],
    );
  }
}
