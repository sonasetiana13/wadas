import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/db/data/company_model.dart';
import 'package:take_home_test/utilitys/view_utils.dart';

class ChooseCompanyController extends BaseController
    with StateMixin<List<CompanyModel>> {
  @override
  void onReady() {
    super.onReady();
    getCompanys();
  }

  Future<void> getCompanys() async {
    try {
      final results = await companyRepo.read();
      change(results,
          status: results.isEmpty ? RxStatus.empty() : RxStatus.success());
    } catch (e) {
      showErrorDatabase();
    }
  }
}
