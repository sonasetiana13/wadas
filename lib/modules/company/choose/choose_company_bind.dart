import 'package:get/get.dart';
import 'package:take_home_test/modules/company/choose/choose_company_control.dart';

class ChooseCompanyBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => ChooseCompanyController());
  }
}
