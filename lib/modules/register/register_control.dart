import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class RegisterController extends BaseController {
  final nameCtrl = TextEditingController();
  final emailCtrl = TextEditingController();
  final phoneCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();

  final errorName = ''.obs;
  final errorEmail = ''.obs;
  final errorPhone = ''.obs;
  final errorPassword = ''.obs;

  final visiblePassword = false.obs;
  final loading = false.obs;

  @override
  void dispose() {
    super.dispose();
    nameCtrl.dispose();
    emailCtrl.dispose();
    phoneCtrl.dispose();
    passwordCtrl.dispose();
  }

  Future<void> register() async {
    final name = nameCtrl.text;
    final email = emailCtrl.text;
    final phone = phoneCtrl.text;
    final password = passwordCtrl.text;

    errorName.value = '';
    errorEmail.value = '';
    errorPhone.value = '';
    errorPassword.value = '';

    if (name.isEmpty) {
      errorName.value = 'Nama tidak boleh kosong';
      return;
    }

    if (email.isEmpty) {
      errorEmail.value = 'Email tidak boleh kosong';
      return;
    }

    if (!email.isEmail) {
      errorEmail.value = 'Email tidak valid';
      return;
    }

    if (phone.isEmpty) {
      errorPhone.value = 'No Telepon tidak boleh kosong';
      return;
    }

    if (!phone.isPhoneNumber) {
      errorPhone.value = 'No Telepon tidak valid';
      return;
    }

    if (password.isEmpty) {
      errorPassword.value = 'Password tidak boleh kosong';
      return;
    }

    if (password.length < 8) {
      errorPassword.value = 'Password min 8 digit';
      return;
    }

    loading.value = true;

    try {
      final result = await userRepo.register(name, email, phone, password);
      if (result) {
        showMessageDialog(
          'Silahkan login untuk masuk ke aplikasi.',
          title: 'Akun Berhasil Dibuat!',
          callback: () {
            Get.offAllNamed(Routes.login);
          },
        );
      } else {
        _showErrorDatabase();
      }
    } catch (e) {
      logI(e.toString());
      _showErrorDatabase();
    } finally {
      loading.value = false;
    }
  }

  void _showErrorDatabase() {
    showMessageDialog(
      'Terjadi kesalhan pada database.',
      title: 'Error!',
    );
  }
}
