import 'package:get/get.dart';
import 'package:take_home_test/modules/register/register_control.dart';

class RegisterBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => RegisterController());
  }
}
