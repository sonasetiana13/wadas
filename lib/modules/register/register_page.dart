import 'package:get/get_state_manager/get_state_manager.dart';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/register/register_control.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/themes/spacing.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/button.dart';
import 'package:take_home_test/widgets/button_loading.dart';
import 'package:take_home_test/widgets/textfield.dart';

class RegisterPage extends GetWidget<RegisterController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BlankAppBar(
        statusBarColor: AppColors.white,
        statusBarIconColor: Brightness.dark,
      ),
      backgroundColor: AppColors.white,
      body: SingleChildScrollView(
        child: SizedBox(
          width: double.infinity,
          height: Get.size.height - 48,
          child: Padding(
            padding: const EdgeInsets.all(Spacing.medium),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Spacer(),
                Text(
                  'Daftar Akun.',
                  style: AppStyles.heading3,
                ),
                Spacing.vSpace32,
                Obx(() => AppTextField(
                      labelText: 'Nama Lengkap',
                      controller: controller.nameCtrl,
                      errorText: TextFieldError(controller.errorName.value),
                      keyboardType: TextInputType.name,
                    )),
                Spacing.vSpace24,
                Obx(() => AppTextField(
                      labelText: 'Email',
                      controller: controller.emailCtrl,
                      errorText: TextFieldError(controller.errorEmail.value),
                      keyboardType: TextInputType.emailAddress,
                    )),
                Spacing.vSpace24,
                Obx(() => AppTextField(
                      labelText: 'No Telepon',
                      controller: controller.phoneCtrl,
                      errorText: TextFieldError(controller.errorPhone.value),
                      keyboardType: TextInputType.phone,
                    )),
                Spacing.vSpace24,
                Obx(() => AppTextField(
                      labelText: 'Password',
                      controller: controller.passwordCtrl,
                      errorText: TextFieldError(controller.errorPassword.value),
                      obscureText: controller.visiblePassword.isFalse,
                      keyboardType: TextInputType.text,
                      suffixIcon: IconButton(
                        splashRadius: 20,
                        onPressed: () {
                          controller.visiblePassword.toggle();
                        },
                        icon: Icon(
                          controller.visiblePassword.isTrue
                              ? Icons.visibility_off
                              : Icons.visibility,
                          color: AppColors.black4,
                        ),
                      ),
                    )),
                Spacing.vSpace32,
                SizedBox(
                  width: double.infinity,
                  child: ButtonLoading(
                    title: 'DAFTAR',
                    loading: controller.loading.value,
                    onPressed: () {
                      controller.register();
                    },
                  ),
                ),
                Spacer(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'Sudah punya akun ?',
                      style: AppStyles.subTextBlack3,
                    ),
                    TextButton(
                        onPressed: () {
                          Get.toNamed(Routes.login);
                        },
                        child: Text(
                          'Masuk',
                          style:
                              AppStyles.bodyBold.update(color: AppColors.green),
                        ))
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
