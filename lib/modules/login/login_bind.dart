import 'package:get/get.dart';
import 'package:take_home_test/modules/login/login_control.dart';

class LoginBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => LoginController());
  }
}
