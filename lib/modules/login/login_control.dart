import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/preference/preference_helper.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class LoginController extends BaseController {
  final emailCtrl = TextEditingController();
  final passwordCtrl = TextEditingController();

  final errorEmail = ''.obs;
  final errorPassword = ''.obs;

  final visiblePassword = false.obs;
  final loading = false.obs;

  @override
  void dispose() {
    super.dispose();
    emailCtrl.dispose();
    passwordCtrl.dispose();
  }

  Future<void> login() async {
    final email = emailCtrl.text;
    final password = passwordCtrl.text;

    errorEmail.value = '';
    errorPassword.value = '';

    if (email.isEmpty) {
      errorEmail.value = 'Email tidak boleh kosong';
      return;
    }

    if (!email.isEmail) {
      errorEmail.value = 'Email tidak valid';
      return;
    }

    if (password.isEmpty) {
      errorPassword.value = 'Password tidak boleh kosong';
      return;
    }

    loading.value = true;

    try {
      final result = await userRepo.login(email, password);
      if (result.isNotEmpty) {
        await PreferenceHelper.setUser(result.first);
        Get.offAllNamed(Routes.home);
      } else {
        showMessageDialog(
          'Silahkan login dengan akun lain.',
          title: 'Akun Tidak Ditemukan!',
        );
      }
    } catch (e) {
      logI(e.toString());
      showErrorDatabase();
    } finally {
      loading.value = false;
    }
  }
}
