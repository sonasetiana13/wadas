import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/splash/splash_control.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_images.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/list_state_widgets.dart';

class SplashPage extends GetWidget<SplashController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      appBar: BlankAppBar(
        statusBarColor: AppColors.white,
        statusBarIconColor: Brightness.dark,
      ),
      body: Stack(
        children: [
          const ListLoadingWidget(),
          Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset(AppImages.imgCityLife)),
        ],
      ),
    );
  }
}
