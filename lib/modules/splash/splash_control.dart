import 'dart:async';

import 'package:get/get.dart';
import 'package:take_home_test/preference/preference_helper.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/view_utils.dart';

class SplashController extends GetxController {
  @override
  void onReady() {
    super.onReady();
    Timer(const Duration(seconds: 2), () async {
      final user = await PreferenceHelper.getUser();
      Get.offAllNamed(user != null ? Routes.home : Routes.login);
    });
  }
}
