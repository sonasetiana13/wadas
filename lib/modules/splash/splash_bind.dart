import 'package:get/get.dart';
import 'package:take_home_test/modules/splash/splash_control.dart';

class SplashBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => SplashController());
  }
}
