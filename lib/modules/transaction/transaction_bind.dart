import 'package:get/get.dart';
import 'package:take_home_test/modules/transaction/transaction_control.dart';

class TransactionBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => TransactionController());
  }
}
