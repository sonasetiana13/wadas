import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/transaction_model.dart';
import 'package:take_home_test/modules/transaction/transaction_control.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/button.dart';

class TransactionPage extends GetWidget<TransactionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: WhiteAppBar(
        titleText: 'Transaksi',
      ),
      body: controller
          .obx((items) => _DataTableTransaction(controller, items: items)),
    );
  }
}

class _DataTableTransaction extends StatelessWidget {
  final TransactionController c;
  final List<TransactionModel>? items;
  const _DataTableTransaction(this.c, {Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaginatedDataTable(
      header: const Text(''),
      columnSpacing: 50,
      horizontalMargin: 10,
      rowsPerPage: 8,
      showCheckboxColumn: false,
      actions: [
        Button.primarySmall(
          title: 'Tambah Transaksi',
          onPressed: () async {
            await Get.toNamed(
              Routes.transactionFrom,
              arguments: {
                'title': 'Tambah Transaksi',
                'button': 'Tambah',
                'type': 'create'
              },
            );
            c.getTransactions();
          },
        )
      ],
      columns: const [
        DataColumn(label: Text('Tanggal')),
        DataColumn(label: Text('Kode Transaksi')),
        DataColumn(label: Text('Nama Barang')),
        DataColumn(label: Text('Nama Perusahaan')),
        DataColumn(label: Text('Total Barang')),
        DataColumn(label: Text('Total Harga')),
        DataColumn(label: Text('Keterangan')),
        DataColumn(label: Text('Hapus')),
        DataColumn(label: Text('Edit'))
      ],
      source: _DataSourceTransaction(c, items: items),
    );
  }
}

class _DataSourceTransaction extends DataTableSource {
  final TransactionController c;
  final List<TransactionModel>? items;
  _DataSourceTransaction(this.c, {this.items});

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => (items ?? []).length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    final item = items![index];
    return DataRow(cells: [
      DataCell(Text(parseDate(item.createdAt))),
      DataCell(Text(item.trxCode ?? '')),
      DataCell(Text(item.productName ?? '')),
      DataCell(Text(item.companyName ?? '')),
      DataCell(Text('${item.totalProduct ?? 0}')),
      DataCell(Text(priceFormat(item.totalPrice))),
      DataCell(Text(item.note ?? '')),
      DataCell(IconButton(
        onPressed: () {
          c.deleteTransaction(
              item.id ?? 0, item.productId ?? 0, item.totalProduct ?? 0);
        },
        icon: const Icon(
          Icons.delete,
          size: 16,
        ),
        splashRadius: 16,
      )),
      DataCell(IconButton(
        onPressed: () async {
          await Get.toNamed(
            Routes.transactionFrom,
            arguments: {
              'title': 'Edit Transaksi',
              'button': 'Edit',
              'type': 'edit',
              'id': item.id,
              'productId': item.productId,
              'companyId': item.companyId,
              'productName': item.productName,
              'companyName': item.companyName,
              'totalPrice': item.totalPrice,
              'totalProduct': item.totalProduct,
              'note': item.note,
            },
          );
          c.getTransactions();
        },
        icon: const Icon(
          Icons.edit,
          size: 16,
        ),
        splashRadius: 16,
      ))
    ]);
  }
}
