import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/db/data/transaction_model.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class TransactionController extends BaseController
    with StateMixin<List<TransactionModel>> {
  @override
  void onReady() {
    super.onReady();
    getTransactions();
  }

  Future<void> getTransactions() async {
    try {
      final results = await transactionRepo.read();
      change(results, status: RxStatus.success());
    } catch (e) {
      showErrorDatabase();
    }
  }

  Future<void> deleteTransaction(int id, int productId, int stock) async {
    showConfirmDialog(
      title: 'Hapus!',
      message: 'Apakah Anda yakin ingin menghapus?',
      callback: () async {
        dissmis();
        try {
          final product = await productRepo.readId(id: productId);
          final result = await productRepo.updateStock(
              id: productId, stock: (product.first.stock ?? 0) + stock);
          if (result) {
            await transactionRepo.delete(id);
            showMessageDialog("Transaksi berhasil dihapus.", title: 'Hapus!');
            getTransactions();
          } else {
            showErrorDatabase();
          }
        } catch (e) {
          logE(e.toString());
          showErrorDatabase();
        }
      },
    );
  }
}
