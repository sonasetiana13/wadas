import 'package:get/get.dart';
import 'package:take_home_test/modules/transaction/form/transaction_form_control.dart';

class FormTransactionBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => FormTransactionController());
  }
}
