import 'package:flutter/material.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/company_model.dart';
import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/dialogs/message_dialog.dart';

class FormTransactionController extends BaseController {
  final productControl = TextEditingController();
  final companyControl = TextEditingController();
  final stockControl = TextEditingController();
  final noteControl = TextEditingController();

  bool _isTypeEdit = false;

  final errorProduct = ''.obs;
  final errorCompany = ''.obs;
  final errorStock = ''.obs;

  final enableStock = false.obs;
  final totalPrice = 0.obs;
  int totalStockEdit = 0;

  ProductModel? product;
  CompanyModel? company;

  @override
  void onInit() {
    _isTypeEdit = Get.arguments['type'] == 'edit';
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
    if (_isTypeEdit) {
      company = CompanyModel(
          id: Get.arguments['companyId'], name: Get.arguments['companyName']);
      companyControl.text = company?.name ?? '';
      totalStockEdit = Get.arguments['totalProduct'];
      stockControl.text = Get.arguments['totalProduct'].toString();
      totalPrice.value = Get.arguments['totalPrice'];
      noteControl.text = Get.arguments['note'] ?? '';
      enableStock.value = true;
      getProduct();
    }
  }

  @override
  void dispose() {
    super.dispose();
    productControl.dispose();
    companyControl.dispose();
    stockControl.dispose();
    noteControl.dispose();
  }

  Future<void> chooseProduct(ProductModel? data) async {
    product = data;
    if (product != null) {
      productControl.text = product?.name ?? '';
      enableStock.value = true;
      errorStock.value = '';
    }
  }

  Future<void> chooseCompany(CompanyModel? data) async {
    company = data;
    if (company != null) {
      companyControl.text = company?.name ?? '';
    }
  }

  void calculatePrice() {
    final stock =
        int.parse(stockControl.text.isEmpty ? '0' : stockControl.text);
    final totalStock = _isTypeEdit
        ? (totalStockEdit > stock
            ? totalStockEdit - stock
            : stock - totalStockEdit)
        : stock;
    logI(totalStock.toString());
    if (product != null) {
      errorStock.value = '';
      if (totalStock > (product?.stock ?? 0)) {
        errorStock.value = 'Stok barang tidak mencukupi';
        totalPrice.value = 0;
        return;
      }
      totalPrice.value = totalStock * (product?.price ?? 0);
    }
  }

  Future<void> getProduct() async {
    final result = await productRepo.readId(id: Get.arguments['productId']);
    if (result.isNotEmpty) {
      product = result.first;
      productControl.text = product?.name ?? '';
    }
  }

  Future<void> save() async {
    final productName = productControl.text;
    final companyName = companyControl.text;
    final stock = stockControl.text;
    final note = noteControl.text;

    errorProduct.value = '';
    errorCompany.value = '';
    errorStock.value = '';

    if (productName.isEmpty) {
      errorProduct.value = 'Anda belum memilih barang';
      return;
    }

    if (companyName.isEmpty) {
      errorCompany.value = 'Anda belum memilih perusahaan';
      return;
    }

    if (stock.isEmpty) {
      errorStock.value = 'Jumlah barang tidak boleh kosong';
      return;
    }
    showConfirmDialog(
      title: 'Konfirmasi!',
      message: 'Pastikan data yang Anda masukkan sudah benar.',
      labelButton: 'Ya',
      callback: () async {
        dissmis();
        try {
          bool result = false;
          if (_isTypeEdit) {
            result = await transactionRepo.update(Get.arguments['id'],
                productId: product?.id,
                companyId: company?.id,
                totalProduct: int.parse(stock.isEmpty ? '0' : stock),
                totalPrice: totalPrice.value,
                note: note);
          } else {
            result = await transactionRepo.create(
                productId: product?.id,
                companyId: company?.id,
                totalProduct: int.parse(stock.isEmpty ? '0' : stock),
                totalPrice: totalPrice.value,
                note: note);
          }

          if (result) {
            _updateStockProduct();
          } else {
            showErrorDatabase();
          }
        } catch (e) {
          logE(e.toString());
          showErrorDatabase();
        }
      },
    );
  }

  Future<void> _updateStockProduct() async {
    final stock =
        int.parse(stockControl.text.isEmpty ? '0' : stockControl.text);
    final totalStockProduct = (product?.stock ?? 0);
    int totalStock = 0;

    if (_isTypeEdit) {
      if (totalStockEdit > stock) {
        totalStock = totalStockProduct + (totalStockEdit - stock);
      } else {
        totalStock = totalStockProduct - (stock - totalStockEdit);
      }
    } else {
      totalStock = totalStockProduct - stock;
    }
    final result =
        await productRepo.updateStock(id: product?.id, stock: totalStock);

    if (result) {
      if (!_isTypeEdit) {
        product = null;
        company = null;
        enableStock.value = false;
        totalPrice.value = 0;
        productControl.text = '';
        companyControl.text = '';
        stockControl.text = '';
        noteControl.text = '';
      }
      showMessageDialog(
          'Transaksi berhasil ${_isTypeEdit ? 'diedit' : 'disimpan'}.',
          title: _isTypeEdit ? 'Edit!' : 'Simpan!');
    } else {
      showErrorDatabase();
    }
  }
}
