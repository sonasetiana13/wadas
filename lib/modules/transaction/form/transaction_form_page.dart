import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/transaction/form/transaction_form_control.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_icons.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/themes/spacing.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/button.dart';
import 'package:take_home_test/widgets/textfield.dart';

class FormTransactionPage extends GetWidget<FormTransactionController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.white,
        appBar: WhiteAppBar(
          titleText: Get.arguments['title'],
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(Spacing.medium),
              child: Column(
                children: [
                  Obx(() => AppTextField(
                        labelText: 'Pilih Barang',
                        controller: controller.productControl,
                        errorText:
                            TextFieldError(controller.errorProduct.value),
                        suffixIcon: Image.asset(
                          AppIcons.icDownArrow,
                          height: 24,
                        ),
                        onTap: () async {
                          final result = await Get.toNamed(Routes.productList);
                          controller.chooseProduct(result);
                        },
                      )),
                  Spacing.vSpace24,
                  Obx(() => AppTextField(
                        labelText: 'Pilih Perusahaan',
                        controller: controller.companyControl,
                        errorText:
                            TextFieldError(controller.errorCompany.value),
                        suffixIcon: Image.asset(
                          AppIcons.icDownArrow,
                          height: 24,
                        ),
                        onTap: () async {
                          final result = await Get.toNamed(Routes.companyList);
                          controller.chooseCompany(result);
                        },
                      )),
                  Spacing.vSpace24,
                  Obx(() => AppTextField(
                        labelText: 'Jumlah Barang',
                        controller: controller.stockControl,
                        errorText: TextFieldError(controller.errorStock.value),
                        keyboardType: TextInputType.number,
                        enabled: controller.enableStock.value,
                        onChange: (value) {
                          controller.calculatePrice();
                        },
                        onTap: controller.enableStock.isTrue
                            ? null
                            : () {
                                controller.errorStock.value =
                                    'Pilih barang terlebih dahulu';
                              },
                      )),
                  Spacing.vSpace24,
                  AppTextField(
                    labelText: 'Keterangan',
                    keyboardType: TextInputType.text,
                    controller: controller.noteControl,
                  ),
                ],
              ),
            ),
            Spacer(),
            Divider(
              thickness: 1.2,
            ),
            Container(
              color: AppColors.white,
              padding: const EdgeInsets.symmetric(
                  vertical: Spacing.normal, horizontal: Spacing.medium),
              child: Row(
                children: [
                  Expanded(
                      flex: 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          const Text(
                            'Total Harga',
                            style: AppStyles.text,
                          ),
                          Obx(() => Text(
                                'Rp ${priceFormat(controller.totalPrice.value)}',
                                style: AppStyles.title,
                              )),
                        ],
                      )),
                  Expanded(
                      flex: 1,
                      child: Button.primary(
                        title: Get.arguments['button'],
                        onPressed: () {
                          controller.save();
                        },
                      )),
                ],
              ),
            ),
          ],
        ));
  }
}
