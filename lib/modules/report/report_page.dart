import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/report/report_control.dart';
import 'package:take_home_test/modules/report/tabs/tabs_company_page.dart';
import 'package:take_home_test/modules/report/tabs/tabs_product_page.dart';
import 'package:take_home_test/modules/report/tabs/tabs_transaction_page.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/widgets/app_bar.dart';

class ReportPage extends GetView<ReportController> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: WhiteAppBar(
          titleText: 'Laporan',
          tabs: const TabBar(
            labelColor: AppColors.black,
            unselectedLabelColor: AppColors.black3,
            indicatorColor: AppColors.green,
            tabs: [
              Tab(
                text: 'Transaksi',
              ),
              Tab(
                text: 'Perusahaan',
              ),
              Tab(
                text: 'Barang',
              ),
            ],
          ),
        ),
        body: const TabBarView(
          children: [
            TabTransactionPage(),
            TabCompanyPage(),
            TabProductPage(),
          ],
        ),
      ),
    );
  }
}
