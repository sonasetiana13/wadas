import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/transaction_model.dart';
import 'package:take_home_test/modules/transaction/transaction_control.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/button.dart';

class TabTransactionPage extends GetWidget<TransactionController> {
  const TabTransactionPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller
        .obx((items) => _DataTableTransaction(controller, items: items));
  }
}

class _DataTableTransaction extends StatelessWidget {
  final TransactionController c;
  final List<TransactionModel>? items;
  const _DataTableTransaction(this.c, {Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaginatedDataTable(
      header: const Text(''),
      columnSpacing: 50,
      horizontalMargin: 10,
      rowsPerPage: 8,
      showCheckboxColumn: false,
      actions: [
        Button.primarySmall(
          title: 'Cetak',
          onPressed: () async {
            List<List<dynamic>> data = [];
            List<dynamic> header = [];
            header.add('Tanggal');
            header.add('Kode Transaksi');
            header.add('Nama Barang');
            header.add('Nama Perusahaan');
            header.add('Total Barang');
            header.add('Total Harga');
            header.add('Keterangan');
            data.add(header);
            for (int i = 0; i < items!.length; i++) {
              List<dynamic> rows = [];
              final item = items![i];
              rows.add(parseDate(item.createdAt));
              rows.add(item.trxCode);
              rows.add(item.productName);
              rows.add(item.companyName);
              rows.add(item.totalProduct);
              rows.add(item.totalPrice);
              rows.add(item.note);
              data.add(rows);
            }
            exportDataToCsv(data, 'Data Transaksi ');
          },
        )
      ],
      columns: const [
        DataColumn(label: Text('Tanggal')),
        DataColumn(label: Text('Kode Transaksi')),
        DataColumn(label: Text('Nama Barang')),
        DataColumn(label: Text('Nama Perusahaan')),
        DataColumn(label: Text('Total Barang')),
        DataColumn(label: Text('Total Harga')),
        DataColumn(label: Text('Keterangan')),
      ],
      source: _DataSourceTransaction(c, items: items),
    );
  }
}

class _DataSourceTransaction extends DataTableSource {
  final TransactionController c;
  final List<TransactionModel>? items;
  _DataSourceTransaction(this.c, {this.items});

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => (items ?? []).length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    final item = items![index];
    return DataRow(cells: [
      DataCell(Text(parseDate(item.createdAt))),
      DataCell(Text(item.trxCode ?? '')),
      DataCell(Text(item.productName ?? '')),
      DataCell(Text(item.companyName ?? '')),
      DataCell(Text('${item.totalProduct ?? 0}')),
      DataCell(Text(priceFormat(item.totalPrice))),
      DataCell(Text(item.note ?? '')),
    ]);
  }
}
