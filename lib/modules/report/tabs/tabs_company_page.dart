import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/db/data/company_model.dart';
import 'package:take_home_test/modules/company/company_control.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/button.dart';

class TabCompanyPage extends GetWidget<CompanyController> {
  const TabCompanyPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller
        .obx((items) => _DataTableCompany(controller, items: items));
  }
}

class _DataTableCompany extends StatelessWidget {
  final CompanyController c;
  final List<CompanyModel>? items;
  const _DataTableCompany(this.c, {Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaginatedDataTable(
      header: const Text(''),
      columnSpacing: 50,
      horizontalMargin: 10,
      rowsPerPage: 8,
      showCheckboxColumn: false,
      actions: [
        Button.primarySmall(
          title: 'Cetak',
          onPressed: () async {
            List<List<dynamic>> data = [];
            List<dynamic> header = [];
            header.add('Tanggal');
            header.add('Nama Perusahaan');
            header.add('Email');
            header.add('No Telepon');
            header.add('Alamat');
            data.add(header);
            for (int i = 0; i < items!.length; i++) {
              List<dynamic> rows = [];
              final item = items![i];
              rows.add(parseDate(item.createAt));
              rows.add(item.name);
              rows.add(item.email);
              rows.add(item.phone);
              rows.add(item.address);
              data.add(rows);
            }
            exportDataToCsv(data, 'Data Perusahaan ');
          },
        )
      ],
      columns: const [
        DataColumn(label: Text('Tanggal')),
        DataColumn(label: Text('Nama Perusahaan')),
        DataColumn(label: Text('Email')),
        DataColumn(label: Text('No Telepon')),
        DataColumn(label: Text('Alamat')),
      ],
      source: _DataSourceCompany(c, items: items),
    );
  }
}

class _DataSourceCompany extends DataTableSource {
  final CompanyController c;
  final List<CompanyModel>? items;
  _DataSourceCompany(this.c, {this.items});

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => (items ?? []).length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    final item = items![index];
    return DataRow(cells: [
      DataCell(Text(parseDate(item.createAt))),
      DataCell(Text(item.name ?? '')),
      DataCell(Text(item.email ?? '')),
      DataCell(Text(item.phone ?? '')),
      DataCell(Text(item.address ?? '')),
    ]);
  }
}
