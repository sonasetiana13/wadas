import 'dart:io';

import 'package:csv/csv.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';
import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/modules/product/product_control.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/utilitys/string_utils.dart';
import 'package:take_home_test/utilitys/view_utils.dart';
import 'package:take_home_test/widgets/button.dart';

class TabProductPage extends GetWidget<ProductController> {
  const TabProductPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return controller
        .obx((items) => _DataTableProduct(controller, items: items));
  }
}

class _DataTableProduct extends StatelessWidget {
  final ProductController c;
  final List<ProductModel>? items;
  const _DataTableProduct(this.c, {Key? key, this.items}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PaginatedDataTable(
      header: const Text(''),
      columnSpacing: 50,
      horizontalMargin: 10,
      rowsPerPage: 8,
      showCheckboxColumn: false,
      actions: [
        Button.primarySmall(
          title: 'Cetak',
          onPressed: () async {
            List<List<dynamic>> data = [];
            List<dynamic> header = [];
            header.add('Tanggal');
            header.add('Nama Barang');
            header.add('Stok');
            header.add('Harga');
            data.add(header);
            for (int i = 0; i < items!.length; i++) {
              List<dynamic> rows = [];
              final item = items![i];
              rows.add(parseDate(item.createAt));
              rows.add(item.name);
              rows.add(item.stock);
              rows.add(item.price);
              data.add(rows);
            }
            exportDataToCsv(data, 'Data Barang ');
          },
        ),
      ],
      columns: const [
        DataColumn(label: Text('Tanggal')),
        DataColumn(label: Text('Nama Barang')),
        DataColumn(label: Text('Stok')),
        DataColumn(label: Text('Harga')),
      ],
      source: _DataSourceProduct(c, items: items),
    );
  }
}

class _DataSourceProduct extends DataTableSource {
  final ProductController c;
  final List<ProductModel>? items;
  _DataSourceProduct(this.c, {this.items});

  @override
  bool get isRowCountApproximate => false;
  @override
  int get rowCount => (items ?? []).length;
  @override
  int get selectedRowCount => 0;
  @override
  DataRow getRow(int index) {
    final item = items![index];
    return DataRow(cells: [
      DataCell(Text(parseDate(item.createAt))),
      DataCell(Text(item.name ?? '')),
      DataCell(Text('${item.stock ?? 0}')),
      DataCell(Text(priceFormat(item.price))),
    ]);
  }
}
