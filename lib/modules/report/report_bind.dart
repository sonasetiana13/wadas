import 'package:get/get.dart';
import 'package:take_home_test/modules/company/company_control.dart';
import 'package:take_home_test/modules/product/product_control.dart';
import 'package:take_home_test/modules/report/report_control.dart';
import 'package:take_home_test/modules/transaction/transaction_control.dart';

class ReportBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => ReportController());
    Get.create(() => TransactionController());
    Get.create(() => CompanyController());
    Get.create(() => ProductController());
  }
}
