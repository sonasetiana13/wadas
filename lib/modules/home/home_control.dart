import 'dart:math';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/base/base_controller.dart';
import 'package:take_home_test/db/data/transaction_model.dart';
import 'package:take_home_test/preference/preference_helper.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/utilitys/view_utils.dart';

class HomeController extends BaseController
    with StateMixin<List<TransactionSummary>> {
  final name = ''.obs;
  @override
  void onReady() {
    super.onReady();
    getAccount();
    getTotalTransction();
  }

  Future<void> getAccount() async {
    final user = await PreferenceHelper.getUser();
    name.value = user?.name ?? '';
  }

  Future<void> getTotalTransction() async {
    try {
      final results = await transactionRepo.getTotalTransaction();
      List<TransactionSummary> items = [];
      Random rand = Random();
      List<Color> colors = [
        AppColors.green,
        AppColors.orange,
        AppColors.blue1,
        AppColors.red,
        AppColors.black
      ];
      results.forEach((e) {
        e.color = colors[rand.nextInt(colors.length)];
        items.add(e);
      });
      change(items,
          status: items.isEmpty ? RxStatus.empty() : RxStatus.success());
    } catch (e) {
      logE(e.toString());
    }
  }
}
