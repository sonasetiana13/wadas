import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/home/home_control.dart';
import 'package:take_home_test/preference/preference_helper.dart';
import 'package:take_home_test/routes/app_routes.dart';
import 'package:take_home_test/themes/app_colors.dart';
import 'package:take_home_test/themes/app_icons.dart';
import 'package:take_home_test/themes/app_styles.dart';
import 'package:take_home_test/themes/spacing.dart';
import 'package:take_home_test/widgets/app_bar.dart';
import 'package:take_home_test/widgets/chart/pie_chart_view.dart';
import 'package:take_home_test/widgets/dialogs/comfirm_dialog.dart';
import 'package:take_home_test/widgets/list_state_widgets.dart';

class HomePage extends GetWidget<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.background,
      appBar: BlankAppBar(
        statusBarColor: AppColors.green,
        statusBarIconColor: Brightness.light,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Obx(() => HeaderWidget(
                  name: controller.name.value,
                )),
            ChartWidget(controller),
            Padding(
              padding: const EdgeInsets.all(Spacing.medium),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      child: ItemButton(
                    controller,
                    icon: AppIcons.icTransaction,
                    title: "Transaksi",
                    description: "Tambah transaksi Anda.",
                    route: Routes.transaction,
                  )),
                  Spacing.hSpace16,
                  Expanded(
                      child: ItemButton(
                    controller,
                    icon: AppIcons.icReport,
                    title: 'Laporan',
                    description: "Lihat Laporan transaksi.",
                    route: Routes.report,
                  )),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                bottom: Spacing.medium,
                left: Spacing.medium,
                right: Spacing.medium,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Expanded(
                      child: ItemButton(
                    controller,
                    icon: AppIcons.icCompany,
                    title: 'Perusahaan',
                    description: 'Kelola data perusahaan.',
                    route: Routes.company,
                  )),
                  Spacing.hSpace16,
                  Expanded(
                      child: ItemButton(
                    controller,
                    icon: AppIcons.icProduct,
                    title: 'Barang',
                    description: 'Kelola data \nbarang.',
                    route: Routes.product,
                  )),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class HeaderWidget extends StatelessWidget {
  final String? name;
  const HeaderWidget({Key? key, this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      color: AppColors.green,
      padding: const EdgeInsets.all(Spacing.large),
      child: Column(
        children: [
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
                onPressed: () async {
                  showConfirmDialog(
                    title: 'Keluar!',
                    message: 'Apakah Anda yakin ingin keluar aplikasi ?',
                    callback: () async {
                      await PreferenceHelper.removeUser();
                      Get.offAllNamed(Routes.login);
                    },
                  );
                },
                icon: const Icon(
                  Icons.exit_to_app,
                  color: AppColors.white,
                  size: 32,
                )),
          ),
          Text(
            name ?? '',
            style: AppStyles.title.update(color: AppColors.white),
            textAlign: TextAlign.start,
          ),
        ],
      ),
    );
  }
}

class ChartWidget extends StatelessWidget {
  final HomeController c;
  const ChartWidget(this.c, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(
          top: Spacing.medium, left: Spacing.medium, right: Spacing.medium),
      decoration: BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.circular(Spacing.normal)),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.only(
              top: Spacing.medium,
              left: Spacing.medium,
              right: Spacing.medium,
            ),
            child: Text(
              'Transaksi',
              style: AppStyles.title,
            ),
          ),
          c.obx((summary) => PieChartView(items: summary),
              onEmpty: SizedBox(
                height: 150,
                child: const ListEmptyWidget(),
              ))
        ],
      ),
    );
  }
}

class ItemButton extends StatelessWidget {
  final HomeController c;
  final String? icon, title, description, route;
  const ItemButton(this.c,
      {Key? key, this.icon, this.title, this.description, this.route})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          padding: const EdgeInsets.all(Spacing.medium),
          decoration: BoxDecoration(
            color: AppColors.white,
            borderRadius: BorderRadius.circular(Spacing.normal),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Align(
                alignment: Alignment.topRight,
                child: Image.asset(
                  icon ?? '',
                  height: 48,
                ),
              ),
              Spacing.vSpace32,
              Text(
                title ?? '',
                style: AppStyles.bodyBold,
              ),
              Text(
                description ?? '',
                style: AppStyles.subText,
              ),
            ],
          ),
        ),
        Positioned.fill(child: GestureDetector(
          onTap: () async {
            await Get.toNamed(route ?? '');
            c.getTotalTransction();
          },
        ))
      ],
    );
  }
}
