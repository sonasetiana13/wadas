import 'package:get/get.dart';
import 'package:take_home_test/modules/home/home_control.dart';

class HomeBinding implements Bindings {
  @override
  void dependencies() {
    Get.create(() => HomeController());
  }
}
