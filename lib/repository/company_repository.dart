import 'package:take_home_test/db/data/company_model.dart';
import 'package:take_home_test/db/db_helper.dart';
import 'package:take_home_test/repository/mapper/company_mapper.dart';
import 'package:take_home_test/utilitys/view_utils.dart';

class CompanyRepository {
  final String _tableName = 'perusahaan';
  Future<bool> create(String name, email, phone, address) async {
    final db = await DBHelper.db();
    return await db.insert(_tableName, {
          'name': name,
          'email': email,
          'phone': phone,
          'address': address,
        }) >
        0;
  }

  Future<List<CompanyModel>> read() async {
    final db = await DBHelper.db();

    final results = await db.query(_tableName, orderBy: "id");
    return results.isEmpty ? [] : CompanyMapper.listMapCompany(results);
  }

  Future<bool> update(int id, String name, email, phone, address) async {
    final db = await DBHelper.db();
    return await db.update(
          _tableName,
          {
            'name': name,
            'email': email,
            'phone': phone,
            'address': address,
            'createdAt': DateTime.now().toString(),
          },
          where: "id = ?",
          whereArgs: [id],
        ) >
        0;
  }

  Future<void> delete(int id) async {
    final db = await DBHelper.db();
    try {
      await db.delete(_tableName, where: "id = ?", whereArgs: [id]);
    } catch (e) {
      logE(e.toString());
    }
  }
}
