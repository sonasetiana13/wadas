import 'package:take_home_test/db/data/user_model.dart';
import 'package:take_home_test/db/db_helper.dart';
import 'package:take_home_test/repository/mapper/user_mapper.dart';

class UserRepository {
  Future<bool> register(String name, email, phone, password) async {
    final db = await DBHelper.db();
    return await db.insert('users', {
          'name': name,
          'email': email,
          'phone': phone,
          'password': password,
        }) >
        0;
  }

  Future<List<UserModel>> login(String email, password) async {
    final db = await DBHelper.db();
    final result = await db.query('users',
        where: "email = ? AND password = ?", whereArgs: [email, password]);
    return result.isEmpty ? [] : UserMapper.listMapUsers(result);
  }
}
