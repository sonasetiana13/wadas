import 'package:intl/intl.dart';
import 'package:take_home_test/db/data/transaction_model.dart';
import 'package:take_home_test/db/db_helper.dart';
import 'package:take_home_test/repository/mapper/transaction_mapper.dart';
import 'package:take_home_test/utilitys/view_utils.dart';

class TransactionRepository {
  final String _tableName = 'transaksi';
  Future<bool> create({
    int? productId,
    int? companyId,
    int? totalProduct,
    int? totalPrice,
    String? note,
  }) async {
    final db = await DBHelper.db();
    return await db.insert(_tableName, {
          'id_barang': productId,
          'id_perusahaan': companyId,
          'total_barang': totalProduct,
          'total_harga': totalPrice,
          'keterangan': note,
          'trx_code': generateTrxCode(),
        }) >
        0;
  }

  Future<List<TransactionModel>> read() async {
    final db = await DBHelper.db();

    final results = await db.rawQuery(
        'SELECT transaksi.id AS trxId, transaksi.id_barang AS productId, transaksi.id_perusahaan AS companyId, transaksi.trx_code, transaksi.total_barang, transaksi.total_harga, transaksi.keterangan, transaksi.createdAt, barang.name AS nama_barang, perusahaan.name AS nama_perusahaan FROM transaksi LEFT JOIN barang ON transaksi.id_barang = barang.id LEFT JOIN perusahaan ON transaksi.id_perusahaan = perusahaan.id');
    return results.isEmpty ? [] : TransactionMapper.listMapTransaction(results);
  }

  Future<bool> update(
    int id, {
    int? productId,
    int? companyId,
    int? totalProduct,
    int? totalPrice,
    String? note,
  }) async {
    final db = await DBHelper.db();
    return await db.update(
          _tableName,
          {
            'id_barang': productId,
            'id_perusahaan': companyId,
            'total_barang': totalProduct,
            'total_harga': totalPrice,
            'keterangan': note,
            'createdAt': DateTime.now().toString(),
          },
          where: "id = ?",
          whereArgs: [id],
        ) >
        0;
  }

  Future<void> delete(int id) async {
    final db = await DBHelper.db();
    try {
      await db.delete(_tableName, where: "id = ?", whereArgs: [id]);
    } catch (e) {
      logE(e.toString());
    }
  }

  Future<List<TransactionSummary>> getTotalTransaction() async {
    final db = await DBHelper.db();
    final results = await db.rawQuery(
        "SELECT SUM(transaksi.total_harga) AS total , SUM(transaksi.total_barang) AS stock , transaksi.id_barang AS productId, transaksi.createdAt AS tanggal, barang.name AS nama_barang FROM transaksi LEFT JOIN barang ON transaksi.id_barang = barang.id WHERE transaksi.createdAt >= ? GROUP BY transaksi.id_barang",
        [DateTime.now().millisecondsSinceEpoch - 3600]);

    return results.isEmpty
        ? []
        : TransactionMapper.listMapTransctionSummary(results);
  }
}
