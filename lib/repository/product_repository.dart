import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/db/db_helper.dart';
import 'package:take_home_test/repository/mapper/product_mapper.dart';
import 'package:take_home_test/utilitys/view_utils.dart';

class ProductRepository {
  final String _tableName = 'barang';
  Future<bool> create(String name, int price, int stock) async {
    final db = await DBHelper.db();
    return await db.insert(_tableName, {
          'name': name,
          'price': price,
          'stock': stock,
        }) >
        0;
  }

  Future<List<ProductModel>> read() async {
    final db = await DBHelper.db();

    final results = await db.query(_tableName, orderBy: "id");
    return results.isEmpty ? [] : ProductMapper.listMapProduct(results);
  }

  Future<List<ProductModel>> readId({int? id}) async {
    final db = await DBHelper.db();

    final results =
        await db.query(_tableName, where: "id = ?", whereArgs: [id]);
    return results.isEmpty ? [] : ProductMapper.listMapProduct(results);
  }

  Future<bool> update(int id, String name, int price, int stock) async {
    final db = await DBHelper.db();
    return await db.update(
          _tableName,
          {
            'name': name,
            'price': price,
            'stock': stock,
            'createdAt': DateTime.now().toString(),
          },
          where: "id = ?",
          whereArgs: [id],
        ) >
        0;
  }

  Future<bool> updateStock({int? id, int? stock}) async {
    final db = await DBHelper.db();
    return await db.update(
          _tableName,
          {
            'stock': stock,
            'createdAt': DateTime.now().toString(),
          },
          where: "id = ?",
          whereArgs: [id],
        ) >
        0;
  }

  Future<void> delete(int id) async {
    final db = await DBHelper.db();
    try {
      await db.delete(_tableName, where: "id = ?", whereArgs: [id]);
    } catch (e) {
      logE(e.toString());
    }
  }
}
