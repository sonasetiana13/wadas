import 'package:take_home_test/db/data/product_model.dart';
import 'package:take_home_test/db/data/user_model.dart';

class ProductMapper {
  ProductMapper._();

  static ProductModel mapProduct(dynamic e) {
    return ProductModel(
      id: e['id'],
      name: e['name'],
      price: e['price'],
      stock: e['stock'],
      createAt: e['createdAt'],
    );
  }

  static List<ProductModel> listMapProduct(dynamic e) {
    return (e as List<dynamic>).map((e) => mapProduct(e)).toList();
  }
}
