import 'package:take_home_test/db/data/company_model.dart';

class CompanyMapper {
  CompanyMapper._();

  static CompanyModel mapCompany(dynamic e) {
    return CompanyModel(
      id: e['id'],
      name: e['name'],
      email: e['email'],
      phone: e['phone'],
      address: e['address'],
      createAt: e['createdAt'],
    );
  }

  static List<CompanyModel> listMapCompany(dynamic e) {
    return (e as List<dynamic>).map((e) => mapCompany(e)).toList();
  }
}
