import 'package:take_home_test/db/data/user_model.dart';

class UserMapper {
  UserMapper._();

  static UserModel mapUser(dynamic e) {
    return UserModel(
      id: e['id'],
      name: e['name'],
      email: e['email'],
      phone: e['phone'],
      createAt: e['createAt'],
    );
  }

  static List<UserModel> listMapUsers(dynamic e) {
    return (e as List<dynamic>).map((e) => mapUser(e)).toList();
  }
}
