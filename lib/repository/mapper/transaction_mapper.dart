import 'package:take_home_test/db/data/transaction_model.dart';

class TransactionMapper {
  TransactionMapper._();

  static TransactionModel mapTransaction(dynamic e) {
    return TransactionModel(
      id: e['trxId'],
      productId: e['productId'],
      companyId: e['companyId'],
      trxCode: e['trx_code'],
      totalPrice: e['total_harga'],
      totalProduct: e['total_barang'],
      note: e['keterangan'],
      createdAt: e['createdAt'],
      productName: e['nama_barang'],
      companyName: e['nama_perusahaan'],
    );
  }

  static TransactionSummary mapTransctionSummary(dynamic e) {
    return TransactionSummary(
      id: e['productId'],
      price: e['total'],
      stock: e['stock'],
      name: e['nama_barang'],
      createdAt: e['tanggal'],
    );
  }

  static List<TransactionModel> listMapTransaction(dynamic e) {
    return (e as List<dynamic>).map((e) => mapTransaction(e)).toList();
  }

  static List<TransactionSummary> listMapTransctionSummary(dynamic e) {
    return (e as List<dynamic>).map((e) => mapTransctionSummary(e)).toList();
  }
}
