class AppIcons {
  AppIcons._();

  static const icCompany = 'assets/icons/ic_company.png';
  static const icProduct = 'assets/icons/ic_product.png';
  static const icReport = 'assets/icons/ic_report.png';
  static const icTransaction = 'assets/icons/ic_transaction.png';
  static const icDownArrow = 'assets/icons/ic_down_arrow.png';
}
