import 'package:flutter/material.dart';
import 'package:take_home_test/themes/app_colors.dart';

class AppStyles {
  AppStyles._();

  static const big = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 48,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const heading3 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 32,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const heading4 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 24,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const subHeading = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 20,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const subHeading2 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 20,
    fontWeight: FontWeight.normal,
    color: AppColors.black,
  );

  static const title = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 18,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const bodyBold = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );

  static const bodyBoldBlack = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const bodyBoldRed = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: AppColors.red,
  );

  static const bodyBoldDarkBrown = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: AppColors.darkBrown,
  );

  static const bodyBoldWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.bold,
    color: AppColors.white,
  );

  static const body = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.black,
  );

  static const bodyWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.white,
  );

  static const bodyBlack3 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.w500,
    color: AppColors.black3,
  );

  static const bodyLight = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 16,
    fontWeight: FontWeight.normal,
    color: AppColors.black,
  );

  static const textBold = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );

  static const textBoldGreen = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: AppColors.green,
  );

  static const textBoldBlack = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const textBoldWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.bold,
    color: AppColors.white,
  );

  static const text = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.w500,
  );

  static const textWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: AppColors.white,
  );

  static const textBlack3 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: AppColors.black3,
  );

  static const textBrown = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: AppColors.brown,
  );

  static const textGreen = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: AppColors.green,
  );

  static const textRed = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.w500,
    color: AppColors.red,
  );

  static const subText = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.normal,
  );

  static const subTextBlack = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: AppColors.black,
  );

  static const subTextWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: AppColors.white,
  );

  static const subTextBlack2 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: AppColors.black2,
  );

  static const subTextBlack3 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 14,
    fontWeight: FontWeight.normal,
    color: AppColors.black3,
  );

  static const label = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.w400,
  );

  static const labelRed = TextStyle(
      fontFamily: 'DMSans',
      fontSize: 12,
      fontWeight: FontWeight.w400,
      color: AppColors.red);

  static const labelDarkBrown = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.darkBrown,
  );

  static const labelBlack3 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.black3,
  );

  static const labelBlack2 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.black2,
  );

  static const labelBold = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.bold,
    color: AppColors.black,
  );

  static const labelBoldBrown = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.bold,
    color: AppColors.brown,
  );

  static const labelBoldDarkBrown = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.bold,
    color: AppColors.darkBrown,
  );

  static const labelMedium = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.w500,
    color: AppColors.black,
  );

  static const labelMediumWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.w500,
    color: AppColors.white,
  );

  static const labelWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 12,
    fontWeight: FontWeight.w400,
    color: AppColors.white,
  );

  static const misc = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w400,
    color: AppColors.black,
  );

  static const miscWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w400,
    color: AppColors.white,
  );

  static const miscBlack2 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w400,
    color: AppColors.black2,
  );

  static const miscBlack3 = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w400,
    color: AppColors.black3,
  );

  static const miscMed = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: AppColors.black,
  );

  static const miscMedWhite = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w500,
    color: AppColors.white,
  );

  static const miscBold = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w400,
  );

  static const miscBoldYellow = TextStyle(
    fontFamily: 'DMSans',
    fontSize: 10,
    fontWeight: FontWeight.w400,
    color: AppColors.yellow,
  );
}

extension Modify on TextStyle {
  TextStyle update({Color? color, FontWeight? weight, double? fontSize}) {
    if (color != null) {
      if (weight != null) {
        if (fontSize != null) {
          return merge(
            TextStyle(color: color, fontWeight: weight, fontSize: fontSize),
          );
        }
        return merge(TextStyle(color: color, fontWeight: weight));
      }
      return merge(TextStyle(color: color));
    } else if (weight != null) {
      if (fontSize != null) {
        return merge(
            TextStyle(color: color, fontWeight: weight, fontSize: fontSize));
      }
      return merge(TextStyle(color: color, fontWeight: weight));
    } else if (fontSize != null) {
      return merge(
          TextStyle(color: color, fontWeight: weight, fontSize: fontSize));
    } else {
      return this;
    }
  }
}
