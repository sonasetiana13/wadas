abstract class Routes {
  static const splash = '/splash';
  static const login = '/login';
  static const register = '/register';
  static const home = '/home';
  static const transaction = '/transaction';
  static const transactionFrom = '/transaction_form';
  static const product = '/product';
  static const productForm = '/product_form';
  static const productList = '/product_list';
  static const company = '/company';
  static const companyForm = '/company_form';
  static const companyList = '/company_list';
  static const report = '/report';
}
