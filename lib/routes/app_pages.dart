import 'package:get/get.dart';
import 'package:take_home_test/modules/company/choose/choose_company_bind.dart';
import 'package:take_home_test/modules/company/choose/choose_company_page.dart';
import 'package:take_home_test/modules/company/company_bind.dart';
import 'package:take_home_test/modules/company/company_page.dart';
import 'package:take_home_test/modules/company/form/company_form_bind.dart';
import 'package:take_home_test/modules/company/form/company_form_page.dart';
import 'package:take_home_test/modules/home/home_bind.dart';
import 'package:take_home_test/modules/home/home_page.dart';
import 'package:take_home_test/modules/login/login_bind.dart';
import 'package:take_home_test/modules/login/login_page.dart';
import 'package:take_home_test/modules/product/choose/choose_product_bind.dart';
import 'package:take_home_test/modules/product/choose/choose_product_page.dart';
import 'package:take_home_test/modules/product/form/product_form_bind.dart';
import 'package:take_home_test/modules/product/form/product_form_page.dart';
import 'package:take_home_test/modules/product/product_bind.dart';
import 'package:take_home_test/modules/product/product_page.dart';
import 'package:take_home_test/modules/register/register_bind.dart';
import 'package:take_home_test/modules/register/register_page.dart';
import 'package:take_home_test/modules/report/report_bind.dart';
import 'package:take_home_test/modules/report/report_page.dart';
import 'package:take_home_test/modules/splash/splash_bind.dart';
import 'package:take_home_test/modules/splash/splash_page.dart';
import 'package:take_home_test/modules/transaction/form/transaction_form_bind.dart';
import 'package:take_home_test/modules/transaction/form/transaction_form_page.dart';
import 'package:take_home_test/modules/transaction/transaction_bind.dart';
import 'package:take_home_test/modules/transaction/transaction_page.dart';
import 'package:take_home_test/routes/app_routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(
      name: Routes.splash,
      page: () => SplashPage(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: Routes.login,
      page: () => LoginPage(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.register,
      page: () => RegisterPage(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: Routes.home,
      page: () => HomePage(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: Routes.transaction,
      page: () => TransactionPage(),
      binding: TransactionBinding(),
    ),
    GetPage(
      name: Routes.transactionFrom,
      page: () => FormTransactionPage(),
      binding: FormTransactionBinding(),
    ),
    GetPage(
      name: Routes.report,
      page: () => ReportPage(),
      binding: ReportBinding(),
    ),
    GetPage(
      name: Routes.product,
      page: () => ProductPage(),
      binding: ProductBinding(),
    ),
    GetPage(
      name: Routes.productForm,
      page: () => FormProductPage(),
      binding: FormProductBinding(),
    ),
    GetPage(
      name: Routes.productList,
      page: () => ChooseProductPage(),
      binding: ChooseProductBinding(),
    ),
    GetPage(
      name: Routes.company,
      page: () => CompanyPage(),
      binding: CompanyBinding(),
    ),
    GetPage(
      name: Routes.companyForm,
      page: () => FormCompanyPage(),
      binding: FormCompanyBinding(),
    ),
    GetPage(
      name: Routes.companyList,
      page: () => ChooseCompanyPage(),
      binding: ChooseCompanyBinding(),
    ),
  ];
}
