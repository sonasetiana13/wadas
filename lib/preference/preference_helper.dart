import 'dart:convert';

import 'package:take_home_test/db/data/user_model.dart';
import 'package:take_home_test/preference/data_preference.dart';
import 'package:take_home_test/repository/mapper/user_mapper.dart';

class PreferenceHelper {
  static const String _USER = 'user';

  static Future setUser(UserModel? data) async {
    String result = json.encode(data!.toJson());
    await DataPreference.putString(_USER, result);
  }

  static Future<UserModel?> getUser() async {
    String? data = await DataPreference.getString(_USER);
    return data == null ? null : UserMapper.mapUser(jsonDecode(data));
  }

  static Future removeUser() async {
    await DataPreference.remove(_USER);
  }
}
