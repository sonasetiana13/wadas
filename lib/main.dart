import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:take_home_test/modules/login/login_bind.dart';
import 'package:take_home_test/modules/splash/splash_page.dart';
import 'package:take_home_test/routes/app_pages.dart';
import 'package:take_home_test/routes/app_routes.dart';

void main() {
  runApp(GetMaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: Routes.splash,
    defaultTransition: Transition.fade,
    initialBinding: LoginBinding(),
    getPages: AppPages.pages,
    home: SplashPage(),
  ));
}
